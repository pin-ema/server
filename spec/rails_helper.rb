# This file is copied to spec/ when you run 'rails generate rspec:install'
require 'spec_helper'
ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'
# Prevent database truncation if the environment is production
abort("The Rails environment is running in production mode!") if Rails.env.production?
require 'rspec/rails'
require 'capybara/rails'

# Add additional requires below this line. Rails is not loaded until this point!

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
#
# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.
#
# Dir[Rails.root.join('spec', 'support', '**', '*.rb')].sort.each { |f| require f }

# Checks for pending migrations and applies them before tests are run.
# If you are not using ActiveRecord, you can remove these lines.
begin
  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end

module RequestSpecHelpers
  def set_current_user! user
    user = case user
      when User then user
      when String, Symbol then users(user)
    end
    get "/authn?user_id=#{user.id}"
    unless response.status == 200
      raise "failed to authenticate user: #{response.body}"
    end
  end

  def parse_body_of response
    body = JSON.parse(response.body)
    body.deep_symbolize_keys!
    body
  end

  def paginated_records total=0, page=1, per_page: 1
    {
      total: total,
      page: page,
      per_page: per_page,
      pages_count: (total.to_f / per_page).ceil
    }
  end

  def map_fixtures name, records
    records.map{ send name, _1 }
  end

  def associated_records **names
    names.keys.each do |name|
      names[name] = names[name].map{ Web::Records.map_associated_record _1 }
    end
    names
  end
end

module SystemSpecHelpers
  def visit_set_current_user! user
    user = case user
      when User then user
      when String, Symbol then users(user)
    end
    visit "/authn?user_id=#{user.id}"
    expect(page).to have_text('OK')
  end
end

Capybara.server = :puma, { Silent: ENV['RSPEC_VERBOSE'].blank? }
Capybara.server_port = ENV['TEST_PORT']
Capybara.app_host = "http://127.0.0.1:#{ENV['TEST_PROXY_PORT']}"

RSpec.configure do |config|
  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # You can uncomment this line to turn off ActiveRecord support entirely.
  # config.use_active_record = false

  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, type: :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!

  # Filter lines from Rails gems in backtraces.
  config.filter_rails_from_backtrace!
  # arbitrary gems may also be filtered via:
  # config.filter_gems_from_backtrace("gem name")

  # custom request helpers
  config.include RequestSpecHelpers, type: :request
  config.include SystemSpecHelpers, type: :system

  config.before type: :system do
    driven_by(
      if ENV['RSPEC_SPECS_SYSTEM_SHOW_BROWSER'].present?
        :selenium_chrome
      else
        :selenium_chrome_headless
      end
    )
  end

  config.around do |example|
    $bug = !!example.metadata[:bug]
    begin
      example.run
    ensure
      $bug = false
    end
  end
end

RSpec::Matchers.define :json_body do |expected|
  match do |response|
    @actual = parse_body_of response
    @actual == expected
  end
  failure_message do
    next 'matcher error' unless @actual
    'Response doesn\'t match'
  end
  failure_message_when_negated do |response|
    next 'matcher error' unless @actual
    [
      'Reponse expected not to match:',
      RSpec::Support::ObjectFormatter.format(response)
    ]. join "\n"
  end
  diffable
end

RSpec::Matchers.define :json_ok do
  match do |response|
    @expected_as_array = [ { ok: true } ]
    @actual = parse_body_of response
    @actual.is_a?(Hash) && @actual[:ok] == true
  end
  failure_message do
    next 'matcher error' unless @actual
    'Response failed'
  end
  failure_message_when_negated do
    next 'matcher error' unless @actual
    'response did not failed'
  end
  diffable
end

RSpec::Matchers.define :json_payload do |expected|
  match do |response|
    @actual = parse_body_of(response)[:payload]
    @actual == expected
  end
  failure_message do
    next 'matcher error' unless @actual
    'Payload doesn\'t match'
  end
  failure_message_when_negated do |response|
    next 'matcher error' unless @actual
    [
      'Payload expected not to match:',
      RSpec::Support::ObjectFormatter.format(response)
    ]. join "\n"
  end
  diffable
end
