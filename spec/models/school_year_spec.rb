# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'SchoolYear', type: :model do
  fixtures :countries

  it 'validates limits' do
    [
      %w[2020-01-01 2020-01-01],
      %w[2020-01-01 2020-01-31],
      %w[2020-01-01 2020-01-31 2021-02-01 2022-02-02],

    ].each do |limits|
      record = SchoolYear.create limits: limits.map{ Date.parse _1 }
      expect(record.errors[:limits]).to be_blank
    end
    [
      %w[2020-01-01],
      %w[2020-01-01 2020-01-31 2020-01-01 2022-02-02],
      %w[2020-01-01 2020-01-01 2020-01-01 2020-01-01],
    ].each do |limits|
      record = SchoolYear.create limits: limits.map{ Date.parse _1 }
      expect(record.errors[:limits]).not_to be_blank
    end
  end

  it 'fills in years field' do
    create = -> (limits) {
      record = SchoolYear.create!(
        country_id: 1,
        name_en: 'a',
        limits: limits.map{ Date.parse _1 }
      )
      record
    }
    expect(create.(%w[2020-01-01 2020-01-31]).year_label).to eq('2020')
    expect(create.(%w[2020-01-01 2021-01-31]).year_label).to eq('2020/21')
  end
end
