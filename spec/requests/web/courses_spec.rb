# frozen_string_literal: true
require 'rails_helper'

RSpec.describe "Web::Courses", type: :request do
  fixtures :all

  describe "GET /index" do
    path = '/api/web/courses'

    def map_record course
      {
        id: course.id,
        education_level_id: course.education_level.id,
        name_en: course.name_en,
        name: course.name,
        grade: course.grade,
      }
    end

    it 'fails when unauthenticated' do
      post path
      expect(response).to json_body({ ok: false, message: 'authn_fail' })
    end

    describe 'restricts access to' do
      it 'root' do
        set_current_user! :root
        post path
        expect(response).to json_ok
      end
      it 'web user' do
        set_current_user! :estonia_admin
        post path
        expect(response).to json_ok
      end
    end

    it 'searches by name' do
      set_current_user! :estonia_admin
      post path, params: { name: 'mart', country_id: 1, per_page: 1 }
      expect(response).to json_ok
      record = map_record(courses(:estonia_history_of_smart_1))
      expect(response).to json_payload(**paginated_records(1), **{
        records: [ record ],
        associations: associated_records(
          education_level: map_fixtures('education_levels', %i[estonia_level_3])
        )
      })
    end
  end
end
