# frozen_string_literal: true
require 'rails_helper'

RSpec.describe "Web::EducationLevels", type: :request do
  fixtures :all

  describe "GET /index" do
    path = '/api/web/education_levels'

    def map_record education_level
      {
        id: education_level.id,
        country_id: education_level.country_id,
        name_en: education_level.name_en,
        name: education_level.name,
        level: education_level.level,
      }
    end

    it 'fails when unauthenticated' do
      post path
      expect(response).to json_body({ ok: false, message: 'authn_fail' })
    end

    describe 'restricts access to' do
      it 'root' do
        set_current_user! :root
        post path
        expect(response).to json_ok
      end
      it 'web user' do
        set_current_user! :estonia_admin
        post path
        expect(response).to json_ok
      end
    end

    it 'searches by name' do
      set_current_user! :estonia_admin
      post path, params: { search: 'conda', country_id: 1, per_page: 1 }
      expect(response).to json_ok
      record = map_record(education_levels(:estonia_level_2))
      expect(response).to json_payload(**paginated_records(1), **{
        records: [ record ],
        associations: associated_records(
          country: map_fixtures('countries', %i[estonia])
        )
      })
    end
  end
end
