# frozen_string_literal: true
require 'rails_helper'

RSpec.describe "Web::EducationLevels", type: :request do
  fixtures :all

  describe "GET /index" do
    path = '/api/web/subjects'

    def map_record subject
      {
        id: subject.id,
        country_id: subject.country_id,
        name_en: subject.name_en,
        name: subject.name,
      }
    end

    it 'fails when unauthenticated' do
      post path
      expect(response).to json_body({ ok: false, message: 'authn_fail' })
    end

    describe 'restricts access to' do
      it 'root' do
        set_current_user! :root
        post path
        expect(response).to json_ok
      end
      it 'web user' do
        set_current_user! :estonia_admin
        post path
        expect(response).to json_ok
      end
    end

    it 'searches by name' do
      set_current_user! :estonia_admin
      post path, params: { search: 'ysi', country_id: 1, per_page: 1 }
      expect(response).to json_ok
      record = map_record(subjects(:estonia_physics))
      expect(response).to json_payload(**paginated_records(1), **{
        records: [ record ],
        associations: associated_records(
          country: map_fixtures('countries', %i[estonia])
        )
      })
    end
  end
end
