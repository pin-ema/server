# frozen_string_literal: true
require 'rails_helper'

RSpec.describe "Web::Persons", type: :request do
  fixtures :all

  describe "GET /index" do
    path = '/api/web/people'

    def map_record course
      {
        id: course.id,
        country_id: course.country_id,
        family_name_en: course.family_name_en,
        family_name: course.family_name,
        given_name_en: course.given_name_en,
        given_name: course.given_name,
        born_on: course.born_on.to_s,
      }
    end

    it 'fails when unauthenticated' do
      post path
      expect(response).to json_body({ ok: false, message: 'authn_fail' })
    end

    describe 'restricts access to' do
      it 'root' do
        set_current_user! :root
        post path
        expect(response).to json_ok
      end
      it 'web user' do
        set_current_user! :estonia_admin
        post path
        expect(response).to json_ok
      end
    end

    it 'searches by name' do
      set_current_user! :estonia_admin
      post path, params: { search: 'rel', country_id: 1, per_page: 1 }
      expect(response).to json_ok
      record = map_record(people(:estonia_karel))
      expect(response).to json_payload(**paginated_records(1), **{
        records: [ record ],
        associations: associated_records(
          country: map_fixtures('countries', %i[estonia])
        )
      })
    end
  end
end
