# frozen_string_literal: true
require 'rails_helper'

RSpec.describe "Web::Schools", type: :request do
  fixtures :all

  describe "GET /index" do
    path = '/api/web/schools'

    def map_record school
      {
        id: school.id,
        country_id: school.country_id,
        name_en: school.name_en,
        name: school.name,
        school_id: school.school_id,
      }
    end

    it 'fails when unauthenticated' do
      post path
      expect(response).to json_body({ ok: false, message: 'authn_fail' })
    end

    describe 'restricts access to' do
      it 'root' do
        set_current_user! :root
        post path
        expect(response).to json_ok
      end
      it 'web user' do
        set_current_user! :estonia_admin
        post path
        expect(response).to json_ok
      end
    end

    it 'searches by name' do
      set_current_user! :estonia_admin
      post path, params: { name: 'mart', country_id: 1, per_page: 1 }
      expect(response).to json_ok
      record = map_record(schools(:estonia_smart_academy))
      expect(response).to json_payload(**paginated_records(1), **{
        records: [ record ],
        associations: associated_records(
          country: map_fixtures('countries', %i[estonia])
        )
      })
    end
  end
end
