# frozen_string_literal: true
require 'rails_helper'

RSpec.describe "Web::Users", type: :request do
  fixtures :all

  describe "GET /index" do
    path = '/api/web/users'

    def map_record user
      {
        id: user.id,
        country_id: user.country_id,
        login: user.login,
        full_name_en: user.full_name_en,
        full_name: user.full_name,
        lock: user.lock,
        last_authn: user.last_authn,
        is_root: user.admissible?(privilege: 'is_root'),
      }
    end

    it 'fails when unauthenticated' do
      post path
      expect(response).to json_body({ ok: false, message: 'authn_fail' })
    end

    describe 'restricts access to' do
      it 'root' do
        set_current_user! :root
        post path
        expect(response).to json_ok
      end
      it 'admin' do
        set_current_user! :estonia_admin
        post path
        expect(response).to json_ok
      end
      it 'web_user' do
        set_current_user! :web_user
        post path
        expect(response).to json_body({ ok: false, message: 'authr_fail' })
      end
    end

    it 'searches by country' do
      set_current_user! :estonia_admin
      post path, params: { country_id: 1, per_page: 1 }
      expect(response).to json_ok
      record = map_record(users(:estonia_admin))
      payload = parse_body_of(response)[:payload]
      record[:last_authn] = payload[:records].first[:last_authn]
      expect(response).to json_payload(**paginated_records(2), **{
        records: [ record ],
        associations: associated_records(
          country: map_fixtures('countries', %i[estonia])
        )
      })
    end

    it 'searches by name' do
      set_current_user! :estonia_admin
      post path, params: { name: 'ser', per_page: 1 }
      expect(response).to json_ok
      record = map_record(users(:estonia_admin))
      payload = parse_body_of(response)[:payload]
      record[:last_authn] = payload[:records].first[:last_authn]
      expect(response).to json_payload(**paginated_records(2), **{
        records: [ record ],
        associations: associated_records(
          country: map_fixtures('countries', %i[estonia])
        )
      })
    end
  end
end
