# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Web::Countries', type: :request do
  fixtures :all

  describe 'GET /index' do
    path = '/api/web/countries'

    def map_record country
      {
        id: country.id,
        name_en: 'Estonia',
        name: country.name
      }
    end

    it 'fails when unauthenticated' do
      post path
      expect(response).to json_body({ ok: false, message: 'authn_fail' })
    end

    describe 'restricts access to' do
      it 'root' do
        set_current_user! :root
        post path
        expect(response).to json_ok
      end
      it 'admin' do
        set_current_user! :estonia_admin
        post path
        expect(response).to json_ok
      end
      it 'web_user' do
        set_current_user! :web_user
        post path
        expect(response).to json_body({ ok: false, message: 'authr_fail' })
      end
    end

    it 'searches by country' do
      set_current_user! :estonia_admin
      post path, params: { search: 'ston', per_page: 1 }
      estonia = countries :estonia
      expect(response).to json_payload(
        **paginated_records(1), **{
          records: [map_record(estonia)],
          associations: nil
        }
      )
    end
  end
end
