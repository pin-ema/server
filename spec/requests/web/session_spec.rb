# frozen_string_literal: true
require 'rails_helper'

RSpec.describe "Web::Session", type: :request do
  fixtures :all

  describe "GET /show" do
    path = '/api/web/session/show'

    def map_countries list
      list.map{ Web::Records.map_associated_record countries(_1) }
    end

    it 'gives karel root access to all countries' do
      set_current_user! :root
      post path
      expect(response).to json_ok
      data = parse_body_of response
      expect(data.dig :payload, :user, :countries).to eq(
        map_countries %i[estonia testland]
      )
    end

    it 'gives user without a designated country no countries' do
      set_current_user! :admin
      post path
      expect(response).to json_ok
      data = parse_body_of response
      expect(data.dig :payload, :user, :countries).to eq([])
    end

    it 'gives estonia admin access to only his countries' do
      set_current_user! :estonia_admin
      post path
      expect(response).to json_ok
      data = parse_body_of response
      expect(data.dig :payload, :user, :countries).to eq(
        map_countries %i[estonia]
      )
    end
  end
end
