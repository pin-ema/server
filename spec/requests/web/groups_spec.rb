# frozen_string_literal: true
require 'rails_helper'

RSpec.describe "Web::Groups", type: :request do
  fixtures :all

  describe "GET /index" do
    path = '/api/web/groups'

    def map_record group
      {
        id: group.id,
        school_id: group.school_id,
        course_id: group.course_id,
        name_en: group.name_en,
        name: group.name,
        year: group.year,
        term: group.term,
      }
    end

    it 'fails when unauthenticated' do
      post path
      expect(response).to json_body({ ok: false, message: 'authn_fail' })
    end

    describe 'restricts access to' do
      it 'root' do
        set_current_user! :root
        post path
        expect(response).to json_ok
      end
      it 'web user' do
        set_current_user! :estonia_admin
        post path
        expect(response).to json_ok
      end
    end

    it 'searches by name' do
      set_current_user! :estonia_admin
      post path, params: { name: 'arels', country_id: 1, per_page: 1 }
      expect(response).to json_ok
      record = map_record(groups(:estonia_karel_group))
      expect(response).to json_payload(**paginated_records(1), **{
        records: [ record ],
        associations: associated_records(
          school: map_fixtures('schools', %i[estonia_smart_academy]),
          course: map_fixtures('courses', %i[estonia_history_of_smart_1])
        )
      })
    end
  end
end
