# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'web database curren_user', type: :system do
  fixtures :all

  it 'allows root to switch country' do
    visit_set_current_user! :root
    visit '/web/database'
    expect(page).to have_text('Estonia')
    expect(page).not_to have_text('Testland')
    find('label', text: 'db.menu.switch_country').click
    dialog = find '.modal-dialog'
    expect(dialog).to have_text('db.menu.switch_country')
    click_button 'Testland'
    expect(page).not_to have_text('Estonia')
    expect(page).to have_text('Testland')
  end

  it 'filters schools per country' do
    school = 'Estonian Academy of Smart'
    visit_set_current_user! :root
    visit '/web/database/schools'
    expect(page).to have_text(school)
    find('label', text: 'db.menu.switch_country').click
    click_button 'Testland'
    expect(page).not_to have_text(school)
  end

end
