# frozen_string_literal: true
require 'rails_helper'

RSpec.describe 'web session', type: :system do
  fixtures :all

  it 'shows not logged in page' do
    visit '/web'
    expect(page).to have_text('app.user_not_present')
  end

  it 'shows homepage for logged user' do
    users(:web_user).update! password: '0Oo0Oo'
    visit '/web'
    find('[data-testid="TopBar--menu"]').click
    click_link 'top_bar.menu.action.login'
    fill_in 'Login', with: 'user'
    fill_in 'Password', with: '0Oo0Oo'
    click_button 'Login'
    expect(page).to have_text('Homepage')
    expect(page).to have_text('Is it any good?')
  end

  it 'allows test function to directly set current user' do
    visit_set_current_user! :estonia_admin
    visit '/web'
    expect(page).to have_text('Homepage')
  end

end
