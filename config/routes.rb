# frozen_string_literal: true

ActionController::Base
Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  root to: ->(env){
    redirector = if Rails.env.development?
      redirect "http://127.0.0.1:#{ENV['PROXY_PORT']}/web", status: 302
    else
      redirect '/web'
    end
    redirector.call env
  }

  get :'favicon.ico', to: -> (_env) { [404, {}, ''] }

  def json_api(**options, &block)
    (options[:defaults] ||= {})[:format] = :json
    (options[:constraints] ||= {})[:format] = :json
    scope(**options, &block)
  end

  def web_controller(controller, as = nil, &block)
    as ||= controller
    as = as.to_s.gsub '/', '_'
    scope controller, controller: controller, as: as, &block
  end

  def web_resource resource_name, skip: []
    scope resource_name, controller: resource_name, as: resource_name do
      post :/, action: :index unless skip.include? :index
      post :create unless skip.include? :create
      scope ':id' do
        post :/, action: :show unless skip.include? :show
        post :update unless skip.include? :update
        post :archive unless skip.include? :archive
      end
      yield if block_given?
    end
  end

  def web_v2_resource resource_name, skip: []
    scope resource_name, controller: resource_name, as: resource_name do
      post :/, action: :index unless skip.include? :index
      post :create unless skip.include? :create
      scope ':id' do
        post :update unless skip.include? :update
      end
      yield if block_given?
    end
  end

  namespace :public do

    controller :application do
      get :countries
    end

  end # /public

  namespace :admin do

    scope :translations, controller: :translations, as: :translations do
      get :'/edit/:language', action: :edit_language, as: 'edit_language'
      post :'/update/:language', action: :update_language, as: 'update_language'
    end

  end # /admin

  namespace :docs do
    get 'dev/mvp-2022', to: redirect(
      'https://docs.google.com/spreadsheets/d/12XzyYzEsjRExS91n6w86UHmGY4DAAtTQbzeEA4UVomc',
      status: 302
    )
  end

  scope :api, as: :api do

    namespace :web do

      namespace :v2 do
        web_v2_resource :donors
        web_v2_resource :material_kits
        web_v2_resource :users do
          scope ':id' do
            post :change_password
            post :lock
          end
        end
        web_v2_resource :work_agreements
      end

      web_controller :session do
        get :'translations/:language', action: :translations, as: :translations
      end

      json_api do
        web_controller :session do
          post :show
          post :login
          post :logout
        end

        web_resource :countries, skip: %i[show create update destroy]

        web_resource :users do
          scope ':id' do
            post :change_password
            post :lock
          end
        end

        web_resource :projects
        web_resource :courses
        web_resource :standardized_courses
        web_resource :education_levels
        web_resource :location_systems, skip: %i[create show update archive]
        web_resource :locations, skip: %i[create show update archive]
        web_resource :material_kits
        web_resource :groups do
          scope ':id' do
            post :show_schedule
            post :update_schedule
          end
        end
        web_resource :people
        web_resource :schools
        web_resource :school_years
        web_resource :subjects
        web_resource :subject_categories
      end
    end

    namespace :android do
      json_api do
        web_controller :session do
          post :login
          post :logout
        end
      end

      web_controller :uploads do
        post :group_attendance_photos
      end
    end

  end

  namespace :server do
    scope :pdf, controller: :pdf, constraints: { format: :pdf }, defaults: { format: :pdf } do
      get :'group_attendance/:group_id', action: :group_attendance
    end
  end

  # backdoor for tests
  if Rails.env.test?
    get 'authn', to: 'application#test_set_authn'
  end
end
