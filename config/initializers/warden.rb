# frozen_string_literal: true

Rails.application.config.middleware.use Warden::Manager do |manager|
  manager.default_strategies :password

  manager.failure_app = ->(env) do
    env['REQUEST_METHOD'] = 'GET'
    ApplicationController.action(:warden_fail_action).call env
  end
end

Warden::Manager.serialize_into_session do |object|
  object.id
end

Warden::Manager.serialize_from_session do |id|
  User.find_by id: id
end

Warden::Manager.after_authentication do |user, auth, opts|
  user.update_column :last_authn, Time.zone.now
  Rails.logger.info "warden authenticated user: #{user.country&.name} - #{user.login}[#{user.id}]"
end

Warden::Strategies.add :password do
  def action_params
    @action_params ||= env['action_dispatch.request.parameters']
  end

  def valid?
    action_params['login'].presence &&
      action_params['password'].presence
  end

  def authenticate!
    table = User.arel_table
    user = User
      .where(login: action_params['login'], lock: nil)
      .where([
        table[:is_root].eq(true),
        table[:country_id].eq(action_params['country'].presence || -1),
      ].reduce :or)
      .first
    if user&.password_matches? action_params['password']
      success! user
    else
      fail!
    end
  end
end

Warden::Strategies.add :android do
  def action_params
    @action_params ||= env['action_dispatch.request.parameters']
  end

  def valid?
    auth = action_params&.dig 'auth'
    auth&.dig('country')&.presence &&
      auth&.dig('token')&.presence
  end

  def authenticate!
    auth = action_params['auth']
    user = User.find_by(
      country_id: auth['country'],
      token: auth['token']
    )
    if user
      success! user
    else
      fail!
    end
  end
end

if Rails.env.test?
  Warden::Strategies.add :direct_set_for_test do
    def action_params
      @action_params ||= env['action_dispatch.request.parameters']
    end

    def authenticate!
      user_id = action_params['user_id'].presence
      user = user_id && User.includes(:country).find_by(id: user_id)
      if user
        success! user
      else
        fail!
      end
    end
  end
end
