# frozen_string_literal: true

if Rails.env.test?
  Rails.application.config.middleware.insert_before 0, Rack::Cors do
    allow do
      origins "http://127.0.0.1:#{ENV['TEST_PROXY_PORT']}"
      resource '*', headers: :any, methods: %i[get post], credentials: true
    end
  end
end

if Rails.env.development?
  Rails.application.config.middleware.insert_before 0, Rack::Cors do
    allow do
      origins "http://127.0.0.1:#{ENV['MOBILE_PORT']}"
      resource '*', headers: :any, methods: %i[get post], credentials: true
    end
  end
end