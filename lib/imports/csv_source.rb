require 'csv'

module Imports
  class CsvSource

    attr_accessor :row_class

    def initialize file_path
      @file_path = file_path
      @cells = []
      @row_class = Imports::CsvSource::Row
      @row_processed = 0
    end

    def read!
      @csv = CSV.open @file_path
    end

    def skip_header! rows=1
      rows.times{ @csv.each.next }
    rescue StopIteration
      # EOF, ignore
    end

    def ignore_cells cells
      cells.each do |literal|
        index = self.class.column_literal_to_i literal.to_s
        @cells[index] = :ignore
      end
    end

    def cell literal, column=nil, &block
      index = self.class.column_literal_to_i literal.to_s
      @cells[index] = column || block
    end

    def process_rows! count=nil
      row_class = self.row_class
      raise 'missing @row_class' unless row_class.is_a? Class

      @cells = Cell.map @cells

      if count.is_a? Numeric
        begin
          count.times do
            row = @csv.each.next
            process_row @cells, row
          end
        rescue StopIteration
          # EOF, ignore
        end

      else
        @csv.each{ process_row @cells, _1 }

      end

      puts to_statistics_text
    end

    class Row < Struct.new(:data)

      attr_reader :record
      attr_accessor :column_pointer

      def cell_value
        data[column_pointer]
      end

      def new_record!
        nil
      end

      def save_record!
        @record&.save!
      end

      def self.with_added_name src, index, value
        name = src || []
        name[index] = value
        name
      end

      def set_name_to attr, index
        record[attr] = self.class.with_added_name(
          record[attr], index, cell_value
        )
      end

      def set_parsed_date_to attr
        value = cell_value
        record[attr] = value.presence && Date.parse(value)
      end

    end

    class Cell < Struct.new(:proc)

      def self.map cells
        cells.map do |column|
          case column
            when :ignore, Cell then column
            when Proc then Cell.new column
          end
        end
      end

    end

    private

    def process_row cells, row
      row = row_class.new row
      row.new_record!
      cells.each_with_index{
        next if _1.nil? || _1 === :ignore
        row.column_pointer = _2
        row.instance_exec &_1.proc
      }
      row.save_record!
      @row_processed += 1
    end

    def to_statistics_text
      [
        'Imports::CsvSourse#to_statistics_text',
        "@total_cells=#{@cells.count}",
        "@skipped_cells=[#{
          @cells
            .map.with_index{ _2 unless _1 }
            .compact
            .map{ self.class.column_i_to_literal _1 }
            .join ','
        }]",
        "@row_processed=#{@row_processed}"
      ].join "\n"
    end

    def self.column_literal_to_i literal
      case literal.length
        when 1 then column_chr_to_i literal
        when 2
          ( 26 * (1 + column_chr_to_i(literal[0])) ) + column_chr_to_i(literal[1])
        else niy!
      end
    end

    def self.column_chr_to_i chr
      chr.ord - 65
    end

    def self.column_i_to_literal index
      if index >= 26
        "#{column_i_to_chr (index / 26) - 1}#{ column_i_to_chr index % 26}"
      else
        column_i_to_chr index
      end
    end

    def self.column_i_to_chr index
      (index + 65).chr
    end

  end
end
