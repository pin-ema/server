require_relative './csv_source'

TMP_PATH = if Rails.env.production?
  Pathname.new '/var/stack_tmp'
else
  Rails.root.join 'tmp/imports'
end

script = ARGV.shift.presence
if not script
  dbg!

else
  script = Rails.root.join "db/imports/#{script}.rb"
  unless File.exist? script
    raise "import script not found: #{script}"
  end
  load script
end
