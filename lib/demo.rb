module Demo

  def self.print_record_seed model, var_name, attrs, count:5
    count.times do |i|
      i += 1
      puts "#{var_name}#{i} = #{model}.create! #{build_attrs attrs, i}"
    end
  end

  def self.build_attrs attrs, index=nil
    attrs = attrs.to_a.map do |key, value|
      value = case value
        when String then value
        when Proc then
          value = value.arity === 1 ? value.call(index) : value.call
          value.is_a?(String) ? value.inspect : value
        else 'nil'
      end
      "#{key}: #{value}"
    end
    attrs.join ', '
  end

  def self.map_records_ids name, indexes
    indexes.map{ get_indexed_record name, _1 }.map(&:id)
  end

  def self.get_indexed_record name, index
    eval "#{name}#{index}"
  end

  def self.fetch_records_ids record, range
    record.all.offset(range.first).limit(range.last - range.first + 1).pluck(:id)
  end

end
