namespace :demo do

  desc 'setups env for the demo'
  task up: :environment do
    # raise 'no' unless Rails.env.development?
    Rake::Task['db:drop'].invoke
    Rake::Task['db:create'].invoke
    Rake::Task['db:schema:load'].invoke
    Rake::Task['db:migrate'].invoke
    Rake::Task['demo:seed'].invoke
  end

  desc 'seeds the db with all default data'
  task seed: :environment do
    Rake::Task['db:fixtures:load'].invoke
    if Rails.env.development?
      User.where(id: [1, 2, 3, 4]).each do |user|
        user.update! password: '0Oo0Oo'
      end
    end
    Rake::Task['demo:load:translations'].invoke
  end

  # desc 'replaces Testland & empty country'
  # task restore_testland: :environment do
  #   Country.find(1)&.destroy
  #   Country.find(2)&.destroy
  #   Rake::Task['db:fixtures:load'].invoke
  #   User.where(id: [2, 3, 4]).each do |user|
  #     user.update! password: '0Oo0Oo'
  #   end
  # end

  desc 'creates new country with defaults'
  task create_country: :environment do
    Country.transaction do
      name = ENV['NAME'].presence or raise 'missing NAME'
      country = Country.create! name: [ name ]
      country.fill_defaults!
    end
  end

  namespace :load do

    desc 'import translations from local sources'
    task translations: :environment do
      Dir[Rails.root.join 'db/translations/*.csv'].each do |file|
        language = File.basename file, '.csv'
        puts "importing translations #{language}"
        TranslationValue.import_language_from_csv language, File.read(file)
      end
    end

    desc 'import syria seeds'
    task syr: :environment do
      load Rails.root.join('db/imports/syria/seed.rb')
    end

  end

end
