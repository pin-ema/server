require 'tty-prompt'
require 'tty-logger'

def task_logger
  @task_logger ||= TTY::Logger.new
end

def task_prompt
  TTY::Prompt.new
end
