module Imports
    class RecordsIndex

      def initialize scope, search
        @scope = scope
        @search = search
        @index = {}
      end

      def [] value
        @index[value] ||= @search.call(@scope, value)
      end

  end
end
