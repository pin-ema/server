require 'csv'

module Imports
  module LocationSystem
    class CountryIndex

      attr_reader :columns_map, :top_location

      # [
      #   [ 0, 1, 2 ] # => level 0 [ label, name_en, name_local ]
      # ]
      def initialize columns_map
        @columns_map = columns_map
        @top_location = Location.new nil, nil, nil
      end

      def parse_file! file, **csv_options
        raise 'not again' if @parsed
        @parsed = true
        csv = CSV.read file, **csv_options
        csv.each(&method(:process_row))
        top_location
      end

      def map_label label, level
        label
      end

      def process_row row
        parent_location = top_location
        last_level = columns_map.length - 1
        columns_map.length.times do |level|
          if level == last_level
            add_location parent_location, row, level
          else
            parent_location = find_or_add_sub_location(
              parent_location, row, level
            )
          end
        end
      end

      def find_or_add_sub_location parent_location, row, level
        col_label, col_name_en, col_name_local = columns_map[level]
        label = map_label row[col_label], level
        parent_location.sub_locations[label] ||= begin
          Location.new label, row[col_name_en], row[col_name_local]
        end
      end

      def add_location parent_location, row, level
        col_label, col_name_en, col_name_local = columns_map[level]
        label = map_label row[col_label], level
        parent_location.sub_locations[label] = Location.new(
          label, row[col_name_en], row[col_name_local]
        )
      end

      class Location < Struct.new(:label, :name_en, :name_local)

        def sub_locations
          @sub_locations ||= {}
        end

        def insert_record! system_id, parent_id, level
          ::Location.create!(
            location_system_id: system_id,
            parent_id: parent_id,
            level: level,
            label: label,
            name: [ name_en, name_local ],
          )
        end

      end

      def insert! locations_system
        recursive_create = -> (location, level, system_id, parent_id) {
          record = location.insert_record! system_id, parent_id, level
          location.sub_locations.values.each do |sub_location|
            recursive_create.call sub_location, level + 1, system_id, record.id
          end
        }
        top_location.sub_locations.values.each do |location|
          recursive_create.call location, 1, locations_system.id, nil
        end
      end

    end
  end
end
