$country = Country.find_by! id: ENV['COUNTRY'].presence
$take_count = ENV['TAKE']&.to_i

$import = Imports::CsvSource.new(
  ENV['FILE'].presence \
    || TMP_PATH.join('syr_students_tracker_SCHF_ENI_SDC_11-2022.csv')
)

$import.row_class = Class.new(Imports::CsvSource::Row) do

  attr_accessor :changed

  def new_record!
    # @record = Person.find_by!(
    #   src: 'syr_import_2022',
    #   student_kobo_no: data[1]
    # )
  end

  def save_record!
    if changed
      record.save!
      self.changed = false
    end
  end

end

$import.cell :M do
  if cell_value == 'Other'
    other_cell_value = data[column_pointer + 1]&.presence
    value = case other_cell_value
      when 'Grandmother' then 'grandmother'
      when 'Grandfather' then 'grandfather'
      when 'Stepmother', 'زوجة الاب' then 'stepmother'
      when 'Stepfather', 'زوج الام' then 'stepfather'
    end
    begin
      record.caregivers[0]['relation'][0] = value
      self.changed = true
    rescue => e
      puts "#{record.id} #{other_cell_value}".red
      puts e.message
      exit 1
    end
  end
end

$country.class.transaction do
  $import.read!
  $import.skip_header! 3
  $import.process_rows! $take_count
end
