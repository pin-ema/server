require 'csv'
require_relative '../lib/records_index'

module Imports
  module Syria
    class StudentTracker

      attr_reader :country, :courses

      def initialize country
        @country = country
        @courses = CoursesList.new
      end

      def self.import! country
        source_csv = File.expand_path '../src/students_tracker.csv', __FILE__
        csv = CSV.read source_csv
        csv.shift # header
        csv.shift # header
        csv.shift # header
        tracker = StudentTracker.new country
        # Country.transaction do
          csv.each do |row|
            tracker.create_student! row
          end
        # end
        tracker.courses.import! country
        nil
      end

      def create_student! row
        _, koboto_id, first_name_en, last_name_en, first_name_local, last_name_local, registered_on, father_first_name_local, mother_first_name_local, mother_last_name_local, caregivers_first_name_local, caregivers_last_name_local, caregivers_relation, caregivers_relation_other, caregivers_phone, caregivers_state_id, enrollment_reasons, spent_outside_school, _, _, _, born_on, _, _, _, gender, residency_status, _, disability_yn, disability_type, disability_type_other, _, _, _, _, _, _, school_name, school_grade_name, school_group_name = row

        person = Person.find_by student_kobo_no: koboto_id
        if person
          puts "skip #{koboto_id}"
          # raise "student #{koboto_id} already in DB"
          # person.destroy
        else
          begin
            person = Person.new(
              country: country,
              person_type: [ 'student' ],
              student_kobo_no: koboto_id,
              born_on: Date.parse(born_on),
              registered_on: registered_on.blank? ? nil : Date.parse(registered_on),
              first_name: [ first_name_en, first_name_local ],
              last_name: [ last_name_en, last_name_local ],
              gender: parse_gender(gender),
              enrollment_reasons: parse_enrollment_reasons(enrollment_reasons),
              residency_status: parse_residency_status(residency_status),
              disabilities: parse_disability(disability_yn, disability_type, disability_type_other),
              outside_school: spent_outside_school,
              caregivers: [
                (caregivers_relation == 'Father' ? nil : {
                  relation: ['father'],
                  first_name: [ '', father_first_name_local ],
                }),
                (caregivers_relation == 'Mother' ? nil : {
                  relation: ['mother'],
                  first_name: [ '', mother_first_name_local ],
                  last_name: [ '', mother_last_name_local ],
                }),
                parse_caregiver(caregivers_first_name_local, caregivers_last_name_local, caregivers_relation, caregivers_relation_other, caregivers_phone, caregivers_state_id),
              ].compact
            )
            person.save!
            puts "imported #{koboto_id}"
          rescue => err
            puts "!!failed #{koboto_id}"
            raise err
          end
        end

        @courses.add_student school_name, school_grade_name, school_group_name, person.id
      end


      def parse_gender value
        case value&.strip&.downcase
          when 'male' then 'm'
          when 'female' then 'f'
        end
      end

      def parse_residency_status value
        case value&.strip
          when 'Host / Resident' then [ 'resident_host' ]
          when 'IDP' then [ 'idp' ]
        end
      end

      def parse_disability yn, type, type_other
        if yn&.downcase&.strip == 'yes'
          case type
            when 'visual impairment', 'Visual- Handicaps' then [ 'visual' ]
            when 'hearing impairment', 'hear- Handicaps' then [ 'hearing' ]
            when 'physical', 'Impaired mobility', 'physical - Handicaps' then [ 'physical' ]
            when 'mental', 'mental - Handicaps', 'Psychological- Handicaps' then [ 'mental' ]
            when 'blindness' then [ 'visual' ]
            when 'other', 'Other'
              case type_other.presence&.downcase
                when 'blindness' then ['visual']
                when nil then ['other']
                else
                  raise "disability other >#{type_other}<"
              end
            else
              raise "disability >#{type}<"
          end
        end
      end

      def parse_caregiver first_name_local, last_name_local, relation, relation_other, phone_no, citizen_id
        relation = case relation&.strip
          when 'Father' then ['father']
          when 'Mother' then ['mother']
          when 'Aunt' then ['aunt']
          when 'Brother' then ['brother']
          when 'Uncle' then ['uncle']
          when 'Other' then ['_other', relation_other]
          else return nil
        end
        {
          relation: relation,
          first_name: [ '', first_name_local ],
          last_name: [ '', last_name_local ],
          phone_no: phone_no,
          citizen_id: citizen_id,
        }
      end

      def parse_enrollment_reasons value
        []
      end

    end

    class CoursesList

      def initialize
        @list = []
      end

      def add_student school, course, group, student
        @list.push(
          CoursesList::Student.new school, course, group, student
        )
      end

      def import! country
        school = School.where(country: country).last!
        school_year = SchoolYear.where(country: country).last!
        @list.each_with_object({}){ (_2[_1.course] ||= []).push _1 }.each do |course_name, students|
          std_course = StandardizedCourse.find_by Web.name_like_sql(StandardizedCourse, course_name)
          unless std_course
            puts "cant find course #{course_name}"
            next
          end

          course = Course.create!(
            school: school,
            education_level: std_course.education_level,
            school_year: school_year,
            standardized_course: std_course,
            grade: std_course.grade,
            name: [course_name],
          )

          students.each_with_object({}){ (_2[_1.group] ||= []).push _1 }.each do |group_name, students|
            Group.create!(
              course: course,
              name: [group_name],
              students_ids: students.map{ _1.student },
              term: 1,
            )
          end
        end
      end

      class Student < Struct.new(:school, :course, :group, :student)

      end

    end
  end
end
