require_relative '../lib/location_system'

module Imports
  module Syria
    class LocationSystem < Imports::LocationSystem::CountryIndex
      def map_label label, level
        case level
          when 0 then label[2, 2]
          when 1 then label[4, 2]
          when 2 then label[6, 2]
          when 3 then label
          else raise
        end
      end

      def self.import! country
        syria_import = new [
          [ 2, 0, 1 ],
          [ 5, 4, 6 ],
          [ 8, 7, 6 ],
          [ 9, 10, 11 ],
        ]
        source_csv = File.expand_path '../src/locations.csv', __FILE__
        raise "no such file #{source_csv}" unless source_csv || !File.exist?(source_csv)
        syria_import.parse_file! source_csv, headers: true

        system = ::LocationSystem.create!(
          country: country,
          label: 'address',
          name: [ 'Address' ],
          levels: 5,
          settings: {
            1 => { type: :s, name: [ 'Governorate' ] },
            2 => { type: :s, name: [ 'District' ] },
            3 => { type: :s, name: [ 'Sub-District' ] },
            4 => { type: :s, name: [ 'Community' ] },
            5 => { type: :t, name: [ 'Camp' ] },
          },
        )

        syria_import.insert! system
        nil
      end
    end
  end
end
