$import = Imports::CsvSource.new ENV['FILE']
$column = ENV['COL']
$list = []

$import.cell $column do
  value = cell_value
  $list.push value unless $list.include? value

  # value = cell_value&.split '/'
  # list = (value || []).map(&:presence).compact
  # list.each do |item|
  #   $list.push item unless $list.include? item
  # end
end

$import.read!
$import.process_rows!

log_object $list
