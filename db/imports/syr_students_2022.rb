$country = Country.find_by! id: ENV['COUNTRY'].presence
$take_count = ENV['TAKE']&.to_i

$import = Imports::CsvSource.new(
  ENV['FILE'].presence \
    || TMP_PATH.join('syr_students_tracker_SCHF_ENI_SDC_11-2022.csv')
)

################################################################################
# Records

$school_year = SchoolYear.find_by! country: $country, id: ENV['YEAR']

$schools = {}
$schools_map = {
  'Abnaa Al-Shohadaa' => 3,
  'Shohadaa Kafar Arouq' => 4,
  'Mustafa Dahman' => 5,
  'Mustafa Kouja' => 6,
  'Bhuri' => 7,
  'Sarmada Al Mohdatha' => 8,
  'Shohadaa Termanin' => 9,
  'Kafr Jalis' => 10,
  'Al-Thuraya' => 11,
  'Al-Israa' => 12,
  'Al-Kalam' => 13,
  'Maktab Al Zaitoun' => 14,
  'Alrahma' => 15,
  'Albayan' => 16,
  'Al Faroq Omar (Al Hasan Bin Ali)' => 17,
  'Al Amal (Al Kawthar)' => 18,
  'Al Hamza (Al Mahaba)' => 19,
  'Al Othmanyah (Moaz Bin Jabal)' => 20,
  'Al Nawaier (Al Ekhlas)' => 21,
  'Atfalona Tonashedokom (Al Wafaa)' => 22,
  'Al Karamah(Shekh Bahr)' => 23,
  'Lastom Wahdakom (Haroun Al Rasheed)' => 24,
  'Omar Bin Alkhatab' => 25,
  'Al Oruba' => 26,
  'Al Nour' => 27,
  'Al Nahdha(Sarmada)(Asma Bin Oumis)' => 28,
  'Baraem Althawra' => 29,
  'Ihsan Aljundi' => 30,
  'Hefsarjah shift 2' => 31,
  'Hefsarjah shift 1' => 32,
  'Kafruhin' => 33,
  'Al-Maali' => 34,
  'Mosa Bin Nosair' => 35,
  'Ghassan Shoeeb' => 36,
  'Yousef Hawara' => 37,
}
def school_by_name name
  $schools[name] ||= begin
    id = $schools_map[name]
    school = id && School.where(country: $country).find(id)
    raise "unknown school #{name}" unless school
    school
  end
end

$courses = {}
$courses_map = {
  'Grade 2' => 3,
  'Grade 1' => 2,
  'Grade 4' => 5,
  'Grade 3' => 4,
  'Grade 7' => 8,
  'Grade 8' => 9,
  'Grade 6' => 7,
  'Grade 5' => 6,
  'Grade 9' => 10,
  'Grade 10' => 11,
  'Grade 11 (scientific)' => 12,
  'Grade 12 (literacy)' => 15,
  'Grade 12 (scientific)' => 14,
  'Grade 11 (literacy)' => 13,
}
def course_by_name school, name
  name = name.strip
  $courses["#{school.id}::#{name}"] ||= begin
    id = $courses_map[name]
    std_course = id && StandardizedCourse
      .includes(:education_level)
      .where(education_level: { country_id: $country.id })
      .find(id)
    raise "unknown std course #{name}" unless std_course
    attrs = {
      school: school,
      education_level: std_course.education_level,
      grade: std_course.grade,
      school_year: $school_year,
    }
    Course.where(attrs).find_by(
      "courses.name[1] = '#{name}'"
    ) \
    || Course.create!({
      **attrs,
      standardized_course: std_course,
      name: [name],
    })
  end
end

$groups = {}
def group_by_name course, name
  $groups["#{course.id}::#{name}"] ||= begin
    attrs = {
      course: course,
      term: 1,
    }
    Group.where(attrs).find_by(
      "groups.name[1] = '#{name}'"
    ) \
    || Group.create!({
      **attrs,
      name: [name],
    })
  end
end

$import.row_class = Class.new(Imports::CsvSource::Row) do
  attr_accessor :school_name, :grade, :classroom

  def new_record!
    @record = Person.new({
      country: $country,
      person_type: [ 'student' ],
      src: 'syr_import_2022',
    })
  end

  def save_record!
    Person.find_by(
      src: 'syr_import_2022',
      student_kobo_no: record.student_kobo_no
    ) and raise 'Cannot import again'

    record.save!

    school = school_by_name school_name
    course = course_by_name school, grade
    group = group_by_name course, classroom
    group.students_ids.push record.id
    group.save!

    # log_object record.attributes.to_a.sort_by(&:first).map{ [ _1.blue, _2 ] }
  end

  def first_caregiver
    caregivers = record.caregivers ||= []
    caregivers[0]||= {}
  end
end

################################################################################
# Columns procs

$import.ignore_cells %i[A] # row in doc
$import.cell(:B){ record.student_kobo_no = cell_value }

$import.cell(:C){ set_name_to :first_name, 0 }
$import.cell(:D){ set_name_to :last_name, 0 }
$import.cell(:E){ set_name_to :first_name, 1 }
$import.cell(:F){ set_name_to :last_name, 1 }
$import.cell(:G){ set_parsed_date_to :registered_on }
$import.cell(:H){ set_name_to :father_first_name, 1 }
$import.cell(:I){ set_name_to :mother_first_name, 1 }
$import.cell(:J){ set_name_to :mother_last_name, 1 }

$import.cell :K do
  name = first_caregiver['first_name']
  first_caregiver['first_name'] = self.class.with_added_name(
    name, 1, cell_value
  )
end
$import.cell :L do
  name = first_caregiver['last_name']
  first_caregiver['last_name'] = self.class.with_added_name(
    name, 1, cell_value
  )
end

cell_M_list = ["Father", "Mother", "Aunt", "Uncle", "Other", "Brother", "Sister", "uncle"]
$import.cell :M do
  value = cell_value
  unless cell_M_list.include? value
    log_object value
    raise 'invalid value'
  end

  first_caregiver['relation'] = [value.downcase]
end

$import.cell :O do
  value = cell_value.to_s
  first_caregiver['phone_no'] = value unless value === '0'
end

$import.cell :P do
  value = cell_value.to_s
  first_caregiver['citizen_id'] = value unless value === '0'
end

$import.cell :R do
  record.outside_school = case cell_value&.strip
    when 'Not out of School' then nil
    when 'less than one month', 'Less than 1 month' then 'less_1_month'
    when '1-2 months' then '1_2_months'
    when '3-6 months' then '3_6_months'
    when '7-11 months' then '7_11_months'
    when '1-2 years' then '1_2_years'
    when 'More than 3 years' then 'more_3_years'
    else raise "invalid value #{cell_value}"
  end
end

$import.ignore_cells %i[S T U] # date
$import.cell(:V){ record.born_on =  Date.parse(cell_value) }
$import.ignore_cells %i[W] # age

$import.cell :Z do
  record.gender = case cell_value&.strip&.downcase
    when 'male' then 'm'
    when 'female' then 'f'
  end
end

$import.ignore_cells %i[AD AE AF AG AH AI]

$import.cell(:AJ){ self.school_name = cell_value }
$import.cell(:AK){ self.grade = cell_value }
$import.cell(:AL){ self.classroom = cell_value }

################################################################################
# Import in transaction

$country.class.transaction do
  $import.read!
  $import.skip_header! 3
  $import.process_rows! $take_count
end
