class CreateSchools < ActiveRecord::Migration[6.1]
  def change

    create_table :schools do |t|
      t.references :country, foreign_key: true, null: false

      t.string :name, array: true
      t.string :external_id
      t.string :address, array: true
      t.bigint :education_levels_ids, array: true
      t.string :education_types, array: true
      t.string :gender_dedications, array: true
      t.integer :classrooms_count
      t.integer :male_latrines_count
      t.integer :female_latrines_count

      t.index %i[country_id external_id], unique: true
    end

    create_table :courses do |t|
      t.references :school, foreign_key: true, null: false
      t.references :education_level, foreign_key: true, null: false
      t.references :school_year, foreign_key: true
      t.references :standardized_course, foreign_key: true
      t.string :name, array: true
      t.date :time_ranges, array: true
      t.boolean :is_formal
      t.integer :grade
      t.string :accreditation_authority, array: true
      t.integer :lesson_duration
      t.integer :attendance_limit
      t.string :preferred_grading, array: true
      t.text :description
      t.jsonb :subjects
    end

  end
end
