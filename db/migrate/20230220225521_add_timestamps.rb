class AddTimestamps < ActiveRecord::Migration[6.1]
  def change

    add_column :schools, :created_at, :datetime
    add_column :schools, :updated_at, :datetime
    add_column :schools, :archived_at, :datetime

    add_column :courses, :created_at, :datetime
    add_column :courses, :updated_at, :datetime
    add_column :courses, :archived_at, :datetime

    add_column :groups, :created_at, :datetime
    add_column :groups, :updated_at, :datetime
    add_column :groups, :archived_at, :datetime

    add_column :people, :created_at, :datetime
    add_column :people, :updated_at, :datetime
    add_column :people, :archived_at, :datetime

  end
end
