class AddPeopleAttrs < ActiveRecord::Migration[6.1]
  def change
    change_table :people do |t|
      t.remove :family_name_en
      t.remove :family_name
      t.remove :given_name_en
      t.remove :given_name

      t.string :person_type, array: true
      t.string :first_name, array: true
      t.string :last_name, array: true
      t.string :gender
      t.string :citizen_id
      t.string :passport_no
      t.string :telephone_no
      t.string :mother_tongue
      t.string :spoken_languages, array: true
      t.string :enrollment_reasons, array: true
      t.date :registered_on
      t.string :outside_school
      t.string :nationality
      t.string :disabilities, array: true
      t.boolean :disability_diagnosis
      t.boolean :assistance_needed
      t.boolean :assistance_provided
      t.text :disability_note
      t.string :residency_status, array: true
      t.integer :school_distance_km
      t.integer :school_distance_min
      t.string :school_transport, array: true
      t.jsonb :caregivers
    end
  end
end
