class AddDirector < ActiveRecord::Migration[6.1]
  def change
    add_reference :schools, :director, foreign_key: { to_table: :people }
  end
end
