class AddPeoplesParents < ActiveRecord::Migration[6.1]
  def change
    add_column :people, :src, :string
    add_column :people, :mother_first_name, :string, array: true
    add_column :people, :mother_last_name, :string, array: true
    add_column :people, :father_first_name, :string, array: true
    add_column :people, :father_last_name, :string, array: true
  end
end
