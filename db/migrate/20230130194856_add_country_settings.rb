class AddCountrySettings < ActiveRecord::Migration[6.1]
  def change
    add_column :countries, :settings, :jsonb
  end
end
