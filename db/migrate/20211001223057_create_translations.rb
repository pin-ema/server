class CreateTranslations < ActiveRecord::Migration[6.1]
  def change

    create_table :translation_values do |t|
      t.string :language
      t.string :key
      t.string :value
      t.boolean :link
      t.index %i[language key], unique: true
    end

    create_table :translation_templates do |t|
      t.string :language, null: false
      t.text :csv
      t.string :version
      t.index %i[language version]
    end

    create_table :system_translations do |t|
      t.string :language
      t.string :key
      t.datetime :created_at
      t.binary :payload
      t.index %i[language key], unique: true
    end

  end
end
