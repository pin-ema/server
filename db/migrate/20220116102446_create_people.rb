class CreatePeople < ActiveRecord::Migration[6.1]
  def change

    create_table :people do |t|
      t.references :country, foreign_key: true, null: false

      t.string :family_name_en, null: false
      t.string :family_name
      t.string :given_name_en, null: false
      t.string :given_name
      t.string :student_kobo_no
      t.date :born_on
    end

    create_table :groups do |t|
      t.references :course, foreign_key: true, null: false
      t.bigint :students_ids, array: true, default: [], null: false

      t.string :name, array: true
      t.integer :term, null: false
    end

    create_table :group_schedules do |t|
      t.references :group
      t.jsonb :subjects
    end

    create_table :attendances do |t|
      t.references :group, foreign_key: true, null: false

      t.string :date

      t.index %i[group_id date], unique: true
    end

    create_table :classifications do |t|
      t.references :group, foreign_key: true, null: false
    end

  end
end
