class AddProjects < ActiveRecord::Migration[6.1]
  def change
    create_table :projects do |t|
      t.references :country, foreign_key: true, null: false
      t.string :name, array: true
      t.string :code
      t.date :starts_on
      t.date :ends_on
      t.text :donors
      t.bigint :schools_ids, array: true
    end
  end
end
