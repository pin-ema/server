class CreateCountry < ActiveRecord::Migration[6.1]
  def change

    create_table :countries do |t|
      t.string :name, array: true
    end

    create_table :users do |t|
      t.references :country, foreign_key: true
      t.string :login, null: false
      t.string :password_digest
      t.string :full_name_en
      t.string :full_name
      t.datetime :last_authn
      t.string :lock
      t.jsonb :privileges
      t.boolean :is_root

      t.index %i[country_id login], unique: true
    end

    create_table :education_levels do |t|
      t.references :country, foreign_key: true, null: false
      t.string :name, array: true
      t.integer :level, null: false

      t.index %i[country_id level], unique: true
    end

    create_table :school_years do |t|
      t.references :country, foreign_key: true, null: false
      t.string :name, array: true
      t.bigint :education_levels, array: true
      t.date :limits, array: true
      t.string :years
      t.boolean :archived
    end

    create_table :standardized_courses do |t|
      t.references :education_level, foreign_key: true, null: false
      t.string :name, array: true
      t.boolean :is_formal
      t.integer :grade
      t.string :accreditation_authority, array: true
      t.integer :lesson_duration
      t.integer :attendance_limit
      t.string :preferred_grading, array: true
      t.text :description
      t.jsonb :subjects
    end

    create_table :subject_categories do |t|
      t.references :country, foreign_key: true, null: false
      t.string :name, array: true
    end

    create_table :subjects do |t|
      t.references :country, foreign_key: true, null: false
      t.string :name, array: true
      t.bigint :education_levels, array: true
      t.bigint :categories, array: true
    end

    create_table :location_systems do |t|
      t.references :country
      t.string :label, null: false
      t.string :name, array: true
      t.integer :levels, null: false
      t.jsonb :settings

      t.index %i[country_id label], unique: true
    end

    create_table :locations do |t|
      t.references :location_system
      t.integer :level
      t.bigint :parent_id
      t.string :label
      t.string :name, array: true

      t.index %i[location_system_id level parent_id label],
        name: 'index_locations_on_label',
        unique: true
    end

  end
end
