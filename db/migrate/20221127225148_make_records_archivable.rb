class MakeRecordsArchivable < ActiveRecord::Migration[6.1]
  def change
    add_column :education_levels, :archived, :boolean
    add_column :standardized_courses, :archived, :boolean
    add_column :schools, :archived, :boolean
    add_column :courses, :archived, :boolean
    add_column :groups, :archived, :boolean
    add_column :people, :archived, :boolean

    add_column :users, :locked_at, :datetime
  end
end
