class AddEntities < ActiveRecord::Migration[6.1]
  def change

    create_table :material_kits do |t|
      t.references :country, foreign_key: true, null: false
      t.string :name, array: true
      t.string :code
      t.text :contents
      t.datetime :created_at
      t.datetime :updated_at
      t.datetime :archived_at
    end

    create_table :donors do |t|
      t.references :country, foreign_key: true, null: false
      t.string :name, array: true
      t.string :code
      t.datetime :created_at
      t.datetime :updated_at
      t.datetime :archived_at
    end

    create_table :work_agreements do |t|
      t.references :country, foreign_key: true, null: false
      t.references :person, foreign_key: true, null: false
      t.bigint :projects_ids, array: true
      t.bigint :donors_ids, array: true
      t.string :position
      t.datetime :starts_on
      t.datetime :ends_on
      t.datetime :resigned_on
      t.text :comment
      t.datetime :created_at
      t.datetime :updated_at
      t.datetime :archived_at
    end

  end
end
