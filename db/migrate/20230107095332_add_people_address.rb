class AddPeopleAddress < ActiveRecord::Migration[6.1]
  def change
    add_column :people, :address, :string, array: true
  end
end
