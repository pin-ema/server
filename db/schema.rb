# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2023_02_20_225521) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "attendances", force: :cascade do |t|
    t.bigint "group_id", null: false
    t.string "date"
    t.index ["group_id", "date"], name: "index_attendances_on_group_id_and_date", unique: true
    t.index ["group_id"], name: "index_attendances_on_group_id"
  end

  create_table "classifications", force: :cascade do |t|
    t.bigint "group_id", null: false
    t.index ["group_id"], name: "index_classifications_on_group_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string "name", array: true
    t.jsonb "settings"
  end

  create_table "courses", force: :cascade do |t|
    t.bigint "school_id", null: false
    t.bigint "education_level_id", null: false
    t.bigint "school_year_id"
    t.bigint "standardized_course_id"
    t.string "name", array: true
    t.date "time_ranges", array: true
    t.boolean "is_formal"
    t.integer "grade"
    t.string "accreditation_authority", array: true
    t.integer "lesson_duration"
    t.integer "attendance_limit"
    t.string "preferred_grading", array: true
    t.text "description"
    t.jsonb "subjects"
    t.boolean "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "archived_at"
    t.index ["education_level_id"], name: "index_courses_on_education_level_id"
    t.index ["school_id"], name: "index_courses_on_school_id"
    t.index ["school_year_id"], name: "index_courses_on_school_year_id"
    t.index ["standardized_course_id"], name: "index_courses_on_standardized_course_id"
  end

  create_table "donors", force: :cascade do |t|
    t.bigint "country_id", null: false
    t.string "name", array: true
    t.string "code"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "archived_at"
    t.index ["country_id"], name: "index_donors_on_country_id"
  end

  create_table "education_levels", force: :cascade do |t|
    t.bigint "country_id", null: false
    t.string "name", array: true
    t.integer "level", null: false
    t.boolean "archived"
    t.index ["country_id", "level"], name: "index_education_levels_on_country_id_and_level", unique: true
    t.index ["country_id"], name: "index_education_levels_on_country_id"
  end

  create_table "group_schedules", force: :cascade do |t|
    t.bigint "group_id"
    t.jsonb "subjects"
    t.index ["group_id"], name: "index_group_schedules_on_group_id"
  end

  create_table "groups", force: :cascade do |t|
    t.bigint "course_id", null: false
    t.bigint "students_ids", default: [], null: false, array: true
    t.string "name", array: true
    t.integer "term", null: false
    t.boolean "archived"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "archived_at"
    t.index ["course_id"], name: "index_groups_on_course_id"
  end

  create_table "location_systems", force: :cascade do |t|
    t.bigint "country_id"
    t.string "label", null: false
    t.string "name", array: true
    t.integer "levels", null: false
    t.jsonb "settings"
    t.index ["country_id", "label"], name: "index_location_systems_on_country_id_and_label", unique: true
    t.index ["country_id"], name: "index_location_systems_on_country_id"
  end

  create_table "locations", force: :cascade do |t|
    t.bigint "location_system_id"
    t.integer "level"
    t.bigint "parent_id"
    t.string "label"
    t.string "name", array: true
    t.index ["location_system_id", "level", "parent_id", "label"], name: "index_locations_on_label", unique: true
    t.index ["location_system_id"], name: "index_locations_on_location_system_id"
  end

  create_table "material_kits", force: :cascade do |t|
    t.bigint "country_id", null: false
    t.string "name", array: true
    t.string "code"
    t.text "contents"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "archived_at"
    t.index ["country_id"], name: "index_material_kits_on_country_id"
  end

  create_table "people", force: :cascade do |t|
    t.bigint "country_id", null: false
    t.string "student_kobo_no"
    t.date "born_on"
    t.string "person_type", array: true
    t.string "first_name", array: true
    t.string "last_name", array: true
    t.string "gender"
    t.string "citizen_id"
    t.string "passport_no"
    t.string "telephone_no"
    t.string "mother_tongue"
    t.string "spoken_languages", array: true
    t.string "enrollment_reasons", array: true
    t.date "registered_on"
    t.string "outside_school"
    t.string "nationality"
    t.string "disabilities", array: true
    t.boolean "disability_diagnosis"
    t.boolean "assistance_needed"
    t.boolean "assistance_provided"
    t.text "disability_note"
    t.string "residency_status", array: true
    t.integer "school_distance_km"
    t.integer "school_distance_min"
    t.string "school_transport", array: true
    t.jsonb "caregivers"
    t.boolean "archived"
    t.string "address", array: true
    t.string "src"
    t.string "mother_first_name", array: true
    t.string "mother_last_name", array: true
    t.string "father_first_name", array: true
    t.string "father_last_name", array: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "archived_at"
    t.index ["country_id"], name: "index_people_on_country_id"
  end

  create_table "projects", force: :cascade do |t|
    t.bigint "country_id", null: false
    t.string "name", array: true
    t.string "code"
    t.date "starts_on"
    t.date "ends_on"
    t.text "donors"
    t.bigint "schools_ids", array: true
    t.index ["country_id"], name: "index_projects_on_country_id"
  end

  create_table "school_years", force: :cascade do |t|
    t.bigint "country_id", null: false
    t.string "name", array: true
    t.bigint "education_levels", array: true
    t.date "limits", array: true
    t.string "years"
    t.boolean "archived"
    t.index ["country_id"], name: "index_school_years_on_country_id"
  end

  create_table "schools", force: :cascade do |t|
    t.bigint "country_id", null: false
    t.string "name", array: true
    t.string "external_id"
    t.string "address", array: true
    t.bigint "education_levels_ids", array: true
    t.string "education_types", array: true
    t.string "gender_dedications", array: true
    t.integer "classrooms_count"
    t.integer "male_latrines_count"
    t.integer "female_latrines_count"
    t.boolean "archived"
    t.bigint "director_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "archived_at"
    t.index ["country_id", "external_id"], name: "index_schools_on_country_id_and_external_id", unique: true
    t.index ["country_id"], name: "index_schools_on_country_id"
    t.index ["director_id"], name: "index_schools_on_director_id"
  end

  create_table "standardized_courses", force: :cascade do |t|
    t.bigint "education_level_id", null: false
    t.string "name", array: true
    t.boolean "is_formal"
    t.integer "grade"
    t.string "accreditation_authority", array: true
    t.integer "lesson_duration"
    t.integer "attendance_limit"
    t.string "preferred_grading", array: true
    t.text "description"
    t.jsonb "subjects"
    t.boolean "archived"
    t.index ["education_level_id"], name: "index_standardized_courses_on_education_level_id"
  end

  create_table "subject_categories", force: :cascade do |t|
    t.bigint "country_id", null: false
    t.string "name", array: true
    t.index ["country_id"], name: "index_subject_categories_on_country_id"
  end

  create_table "subjects", force: :cascade do |t|
    t.bigint "country_id", null: false
    t.string "name", array: true
    t.bigint "education_levels", array: true
    t.bigint "categories", array: true
    t.index ["country_id"], name: "index_subjects_on_country_id"
  end

  create_table "system_translations", force: :cascade do |t|
    t.string "language"
    t.string "key"
    t.datetime "created_at"
    t.binary "payload"
    t.index ["language", "key"], name: "index_system_translations_on_language_and_key", unique: true
  end

  create_table "translation_templates", force: :cascade do |t|
    t.string "language", null: false
    t.text "csv"
    t.string "version"
    t.index ["language", "version"], name: "index_translation_templates_on_language_and_version"
  end

  create_table "translation_values", force: :cascade do |t|
    t.string "language"
    t.string "key"
    t.string "value"
    t.boolean "link"
    t.index ["language", "key"], name: "index_translation_values_on_language_and_key", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.bigint "country_id"
    t.string "login", null: false
    t.string "password_digest"
    t.string "full_name_en"
    t.string "full_name"
    t.datetime "last_authn"
    t.string "lock"
    t.jsonb "privileges"
    t.boolean "is_root"
    t.datetime "locked_at"
    t.string "token"
    t.index ["country_id", "login"], name: "index_users_on_country_id_and_login", unique: true
    t.index ["country_id"], name: "index_users_on_country_id"
  end

  create_table "work_agreements", force: :cascade do |t|
    t.bigint "country_id", null: false
    t.bigint "person_id", null: false
    t.bigint "projects_ids", array: true
    t.bigint "donors_ids", array: true
    t.string "position"
    t.datetime "starts_on"
    t.datetime "ends_on"
    t.datetime "resigned_on"
    t.text "comment"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "archived_at"
    t.index ["country_id"], name: "index_work_agreements_on_country_id"
    t.index ["person_id"], name: "index_work_agreements_on_person_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "attendances", "groups"
  add_foreign_key "classifications", "groups"
  add_foreign_key "courses", "education_levels"
  add_foreign_key "courses", "school_years"
  add_foreign_key "courses", "schools"
  add_foreign_key "courses", "standardized_courses"
  add_foreign_key "donors", "countries"
  add_foreign_key "education_levels", "countries"
  add_foreign_key "groups", "courses"
  add_foreign_key "material_kits", "countries"
  add_foreign_key "people", "countries"
  add_foreign_key "projects", "countries"
  add_foreign_key "school_years", "countries"
  add_foreign_key "schools", "countries"
  add_foreign_key "schools", "people", column: "director_id"
  add_foreign_key "standardized_courses", "education_levels"
  add_foreign_key "subject_categories", "countries"
  add_foreign_key "subjects", "countries"
  add_foreign_key "users", "countries"
  add_foreign_key "work_agreements", "countries"
  add_foreign_key "work_agreements", "people"
end
