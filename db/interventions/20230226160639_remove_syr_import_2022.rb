require Rails.root.join('lib/tasks/lib')

# delete previous import

country = Country.find 3
country.transaction do
  students = Person.where(
    country: country,
    src: 'syr_import_2022',
  )

  students_ids = students.map(&:id)
  task_logger.warn "students=#{students_ids}"

  students.delete_all
  students = nil

  Group
    .includes(course: { school: :country })
    .where(courses: { schools: { country: country } })
    .find_in_batches do |groups|
    groups.each do |group|
      new_students_ids = group.students_ids - students_ids
      if new_students_ids.empty?
        group.destroy!
      else
        group.update! students_ids: new_students_ids
      end
    end
  end

end

task_logger.success 'done'
