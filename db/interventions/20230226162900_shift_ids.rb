require Rails.root.join('lib/tasks/lib')

class ModelChange

  def initialize model
    @model = model
    @associations = []
    @array_associations = []
  end

  def add_association name, assoc_model
    @associations.push({
      name: name,
      assoc_model: assoc_model,
    })

    self
  end

  def add_array_association name, assoc_model
    @array_associations.push({
      name: name,
      assoc_model: assoc_model,
    })

    self
  end

  def swap_ids
    return if @model.count.zero?

    unless @associations.empty? && @array_associations.empty?
      raise "damn - #{@model.name}" if @model.count == 1
    end

    @map = {}

    lowest_id = @model.first.id
    return if lowest_id > 100

    next_id = @model.last.id + 1
    next_id = 101 if next_id <= 100
    @model.where(id: [(lowest_id+1)..100]).each do |record|
      @map[record.id] = next_id
      process_record record, lowest_id, next_id
      next_id += 1
    end
    @model.where(id: lowest_id).each do |record|
      @map[record.id] = next_id
      process_record record, 101, next_id
      next_id += 1
    end

    update_array_assocs

    self
  end

  def delete_records
    execute "DELETE FROM #{@model.table_name};"
    self
  end

  def add_100_id
    next_id = @model.last&.id || 100
    execute "SELECT setval('#{@model.table_name}_id_seq', #{next_id});"
    self
  end

  def process_record record, temp_id, next_id
    @associations.map{ _1[:ids] = _1[:assoc_model].where( _1[:name] => record.id ).pluck(:id) }
    @associations.each{ _1[:assoc_model].where(id: _1[:ids]).update_all _1[:name] => temp_id }
    record.update! id: next_id
    @associations.each{ _1[:assoc_model].where(id: _1[:ids]).update_all _1[:name] => next_id }
  end

  def update_array_assocs
    @map.each do |previous_id, new_id|
      @array_associations.each do |assoc|
        assoc[:assoc_model].where(
          "#{previous_id} = ANY(#{assoc[:name]})"
        ).each do |record|
          if record[:name].delete previous_id
            record[:name.push new_id]
            record.save!
          end
        end
      end
    end
  end

end

def execute query
  ApplicationRecord.connection.exec_query query
end

begin
  ApplicationRecord.transaction do

    execute 'DELETE FROM attendances;'
    execute 'DELETE FROM classifications;'
    execute 'DELETE FROM group_schedules;'

    ModelChange.new(Course).tap{
      _1.add_association :course_id, Group
    }.swap_ids
      .add_100_id

    ModelChange.new(Donor).delete_records
      .add_100_id

    ModelChange.new(EducationLevel).tap{
      _1.add_association :education_level_id, Course
      _1.add_association :education_level_id, StandardizedCourse
    }
      .add_array_association(:education_levels_ids, School)
      .add_array_association(:education_levels, SchoolYear)
      .add_array_association(:education_levels, Subject)
      .swap_ids
      .add_100_id

    ModelChange.new(Group).swap_ids
      .add_100_id

    ModelChange.new(LocationSystem).tap{
      _1.add_association :location_system_id, Location
    }.swap_ids
      .add_100_id

    ModelChange.new(Location).swap_ids
      .add_100_id

    ModelChange.new(MaterialKit)
      .delete_records
      .add_100_id

    ModelChange.new(Person)
      .add_association(:person_id, WorkAgreement)
      .add_association(:director_id, School)
      .add_array_association(:students_ids, Group)
      .swap_ids
      .add_100_id

    ModelChange.new(Project).swap_ids
      .add_100_id

    ModelChange.new(SchoolYear).tap{
      _1.add_association :school_year_id, Course
    }.swap_ids
      .add_100_id

    ModelChange.new(School)
      .tap{
        _1.add_association :school_id, Course
      }
      .add_array_association(:schools_ids, Project)
      .swap_ids
      .add_100_id

    ModelChange.new(StandardizedCourse).tap{
      _1.add_association :standardized_course_id, Course
    }.swap_ids
      .add_100_id

    ModelChange.new(SubjectCategory)
      .delete_records
      .add_100_id

    ModelChange.new(Subject).swap_ids
      .add_100_id

    ModelChange.new(User).swap_ids
      .add_100_id

    ModelChange.new(WorkAgreement)
      .delete_records
      .add_100_id
  end

# rescue => e
#   task_logger.error e.message

end

task_logger.success 'done'
