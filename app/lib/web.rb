# frozen_string_literal: true

module Web
  def self.format_date(date)
    date&.strftime '%-d. %-m. %Y %k:%M:%S'
  end

  def self.build_pagination(page, total, per_page)
    {
      total: total,
      page: page,
      per_page: per_page,
      pages_count: (total.to_f / per_page).ceil
    }
  end

  def self.name_like_sql model, value, attribute: 'name'
    "array_to_string(\"#{model.table_name}\".\"#{attribute}\", ' ') ILIKE '%#{value}%'"
  end
end
