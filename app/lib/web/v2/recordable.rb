module Web::V2::Recordable

  private

  def record_class
    @record_class ||= controller_name.classify.constantize
  end

  def records_scope
    @records_scope ||= begin
      scope = record_class.all.order id: :desc

      selected_country = if (param = params[:country_id].presence&.to_i)
        all_user_countries.find{ param == _1.id }
      end
      @records_scope_countries = if selected_country
        [ selected_country ]
      else
        all_user_countries
      end

      if (param = params[:id].presence&.to_i)
        scope = scope.where id: param
      end

      scope
    end
  end

  def map_a_record record
    { id: record.id.to_s }
  end

  def record_sanitized_params
    params
  end

  def record_change_resp success, record=nil
    unless record
      return respond_ok(
        success: false,
        errors: [ [ :id, :not_found ] ]
      )
    end

    data = {
      success: success && record.errors.empty?,
      record_id: record.id.to_s,
    }

    unless record.errors.empty?
      data[:errors] = record.errors.map do |error|
        [
          error.attribute,
          [ error.type, error.options[:message] ].compact.join('.'),
        ]
      end
    end

    respond_ok **data
  end

  def find_record
    @record ||= records_scope.find_by(id: params[:id])
  end

  def change_record
    record = find_record or return record_change_resp false
    success = yield record
    record_change_resp success, record
  end

  module IndexAction
    DEFAULT_PER_PAGE_OPTIONS = [ 20, 50, 1 ]
    def index
      scope = records_scope
      if respond_to? :index_filtered_scope, true
        scope = index_filtered_scope(scope) || scope
      end

      count = scope.count
      page = params[:page].presence&.to_i || 1
      per_page = params[:per_page].presence&.to_i
      per_page = if IndexAction::DEFAULT_PER_PAGE_OPTIONS.include? per_page
        per_page
      else
        IndexAction::DEFAULT_PER_PAGE_OPTIONS.first
      end
      scope = scope.limit(per_page).offset(( page - 1 ) * per_page)

      records = scope.to_a
      respond_ok(
        records: (params[:b_record] ?
          records.map(&:to_b_record) :
          records.map { map_a_record _1 }
        ),
        **(Web.build_pagination page, count, per_page),
        associations:  if respond_to? :records_associations, true
          records_associations(scope)&.yield_self{
            Web::V2::Recordable.map_b_records_index _1
          }
        end
      )
    end
  end

  module CreateAction
    def create
      sanitized_params = record_sanitized_params
      record = record_class.create sanitized_params
      record_change_resp record.persisted?, record
    end
  end

  module UpdateAction
    def update
      record = find_record or return record_change_resp false
      sanitized_params = record_sanitized_params
      success = record.update sanitized_params
      record_change_resp success, record
    end
  end

  def self.map_b_records_index records_index
    records_index.each_key do |name|
      records_index[name] = records_index[name]
        .map(&:to_b_record)
        .each_with_object({}){ _2.update( _1[:id] => _1) }
    end
    records_index
  end

end
