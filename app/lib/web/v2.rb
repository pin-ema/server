module Web::V2

  class UserNotPresentError < StandardError; end

  class UserNotAuthorizedError < StandardError; end

end
