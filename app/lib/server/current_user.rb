# frozen_string_literal: true

module Server
  module CurrentUser
    extend ActiveSupport::Concern

    included do
      rescue_from UserNotPresentError do |exception|
        log_exception exception, backtrace: false
        user_missing_fail
      end
    end

    private

    def current_user
      @current_user or begin
        raise UserNotPresentError
      end
    end

    def current_user_countries
      @current_user_countries ||= if !current_user
        []
      elsif current_user.is_root?
        Country.all.to_a
      else
        [ current_user.country ].compact
      end
    end

    def current_country
      if (param = params[:country_id].presence&.to_i)
        current_user_countries.find{ param == _1.id }
      else
        current_user_countries.first
      end
    end

    def authenticate!
      @current_user = request.env['warden'].authenticate! :password
    end

    class UserNotPresentError < StandardError; end

  end
end
