# frozen_string_literal: true
require 'prawn'

require_relative '../documents'

class Server::Pdf::Documents::GroupAttendance
  include Server::Pdf::Documents::Helpers

  attr_reader :prawn_doc, :pdf

  def initialize group
    @group = group
    self.country = group.course.school.country
    @prawn_doc = Server::Pdf::PrawnDoc.new(
      'group_attendance',
      page_size: 'A4',
      page_layout: :landscape,
      margin: 0
    )
    @pdf = prawn_doc.pdf
  end

  def generate
    per_page = 19
    pdf.font_size = 10
    students_no_start = 1

    all_students = @group.scope_students.to_a
    pages_count = (all_students.length.to_f / per_page).ceil
    all_students.each_slice(per_page).each_with_index do |students, page|
      pdf.start_new_page unless page.zero?
      header
      attendance_table students, students_no_start
      footer page, pages_count
      students_no_start += students.length
    end
  end

  def header
    school = @group.course.school

    box 0, 51, 842, 89 do
      text_bold(
        "Students' Monthly Attendance",
        [ 10, 1, 822, 17 ],
        size: 12, align: :center
      )

      box 10, 45, 353, 26 do
        text_bold(
          "School /\nLearning center :",
          [ 0, 0, 90, 28 ]
        )
        text(
          "#{school.to_id_text} #{school.name[0]}",
          [ 100, 0, 253, 14 ]
        )
        localized_text(
          school.name[1],
          [ 100, 13, 253, 18 ]
        )
      end

      box 10, 73, 820, 12 do
        text_bold(
          'Location :',
          [ 0, 0, 85, 14 ]
        )
        text(
        school.to_s_address_line,
          [ 100, 0, 253, 14 ]
        )
      end

      box 376, 17, 228, 26 do
        text_bold(
          'Class :',
          [ 0, 0, 78, 26 ]
        )
        text(
          @group.name[0],
          [ 78, 0, 150, 14 ]
        )
        localized_text(
          @group.name[1],
          [ 78, 13, 150, 16 ]
        )
      end

      box 376, 45, 228, 26 do
        text_bold(
          'Course :',
          [ 0, 0, 78, 26 ]
        )
        text(
          "#{@group.course.to_id_text} #{@group.course.name[0]}",
          [ 78, 0, 150, 14 ]
        )
        localized_text(
          @group.course.name[1],
          [ 78, 13, 150, 14 ]
        )
      end

      box 609, 49, 221, 12 do
        text_bold(
          'School year / period :',
          [ 0, 0, 110, 26 ]
        )
        year = @group.course.school_year
        text(
          year && "#{year.to_id_text} #{year.name[0]}",
          [ 110, 0, 111, 14 ]
        )
      end

      box 620, 0, 210, 45 do
        text_bold(
          'Select Month :',
          [ 0, 2, 200, 14 ]
        )
        table [ 0, 15 ] do |t|
          columns = Array.new(13){ 16 }
          t.add_row 14, columns, (
            columns.length.times.map do |num|
              num = "#{num + 1}."
              -> { text num, _1, align: :center, valign: :center }
            end
          )
          t.add_row 16, columns, []
        end
      end

      pdf.stroke_horizontal_line 10, pdf.bounds.width - 10, at: y(88)
    end

    box 772, 7, 60, 60 do
      prawn_doc.with_qr_code(
        'group_attendance',
        'g=123456,p=123456'
      ){ pdf.image _1, position: :center, vposition: :center }
    end
  end

  def attendance_table students, students_no_start
    date_columns = 31.times.map{ 18 }
    table [ 10, 142 ] do |t|
      t.add_row 18, [ 18, 186, 60, *date_columns ], [
        -> { text_bold '#', _1, align: :center, valign: :center },
        -> { text_bold 'Student Name', _1, align: :center, valign: :center },
        -> { text_bold 'ID', _1, align: :center, valign: :center },
        *(31.times.map{ |index|
          label = "#{index + 1}."
          -> { text_bold label, _1, align: :center, valign: :center }
        }),
      ]
      t.add_row 18, [ 18, 246, *(31.times.map{ 18 }) ], [
        nil,
        -> { text_bold 'Class happened', _1, valign: :center },
      ]
      students.each_with_index do |student, row_index|
        t.add_row 18, [ 18, 186, 60, *date_columns ], [
          -> { text (row_index + students_no_start).to_i, _1, align: :center, valign: :center },
          -> { localized_text student.full_name_arabic, _1, align: :center, valign: :center, direction: :rtl },
          -> { text student.to_id_text, _1, align: :center, valign: :center },
        ]
      end
    end
  end

  def footer page, pages_count
    box 0, 529, pdf.bounds.width, 63 do
      text_bold 'Legend :', [ 28, 4, 50, 11 ], size: 8
      legend_item 73, 3, 'X', 'Present'
      legend_item 73, 13, '', 'Absent without reason'
      legend_item 193, 3, 'C', 'Cultural / Religious'
      legend_item 193, 13, 'E', 'Economical / Financial'
      legend_item 313, 3, 'S', 'Security'
      legend_item 313, 13, 'P', 'Political'
      legend_item 378, 3, 'M', 'Child Marriage'
      legend_item 378, 13, 'F', 'Family Reason'
      legend_item 470, 3, 'A', 'Agriculture'
      legend_item 470, 13, 'W', 'Weather'
      legend_item 548, 3, 'H', 'Health / Medical'
      legend_item 548, 13, 'L', 'Labour'
      legend_item 644, 3, 'O', 'Other'

      text_bold 'Approved by PIN staff :', [ 28, 32, 120, 14 ]
      text_bold 'Approved by school director / responsible :',
        [ 391, 32, 210, 14 ]


      pdf.save_graphics_state do
        pdf.fill_color 'E2E2E2'
        pdf.fill_rectangle [ 148, y(28) ], 200, 14
        pdf.fill_rectangle [ 607, y(28) ], 200, 14
      end
      pdf.stroke_horizontal_line 148, 348, at: y(42)
      pdf.stroke_horizontal_line 607, 807, at: y(42)

      position =  [ 10, 42, pdf.bounds.width - 20, 17 ]
      text "Page : #{page + 1} / #{pages_count}",
        position,
        align: :center, valign: :center
      text 'Generated by EMA', position, align: :right, valign: :center
    end
  end

  def legend_item x, y, character, text
    box x, y, 100, 10 do
      table do |t|
        t.add_row 11, [ 10 ], [
          -> { text character, _1, size: 8, align: :center, valign: :center },
        ]
      end
      text text, [ 12, 0, 88, 11 ], size: 8, valign: :center
    end
  end

  def text_bold text, position, **options
    self.text text, position, style: :bold, **options
  end

end
