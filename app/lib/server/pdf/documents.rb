require_relative '../pdf'

module Server::Pdf::Documents

  module Helpers

    def country= country
      @country = country
    end

    def y y
      @pdf.bounds.height - y
    end

    def t key
      "tr:#{key}"
    end

    def box x, y, widht, height, &block
      pdf.bounding_box [ x, y(y) ], width: widht, height: height, &block
    end

    def text text, position, **options
      x, y, width, height = position
      pdf.text_box(
        text.to_s,
        at: [ x, self.y(y) ], width: width, height: height,
        **options
      )
    end

    def localized_text text, position, **options
      if @country&.settings&.[]('arabic')
        text = text&.connect_arabic_letters
        self.text text, position, **options, direction: :rtl
      else
        self.text text, position, **options
      end
    end

    def table position = [0, 0]
      table = Table.new self, position
      if block_given?
        yield table
        table.print
      end
      table
    end

    class Table
      attr_reader :doc

      def initialize doc, position
        @doc = doc
        @position = position
        @rows = []
      end

      def add_row height, columns, content
        @rows.push Row.new(height, columns, content)
      end

      def print
        doc.pdf.save_graphics_state do
          box do
            strike_cells
            print_content
          end
        end
      end

      private

      def box &block
        width = @rows.map{ _1.columns.sum }.max
        height = @rows.map{ _1.height }.sum
        x, y = @position
        doc.box x, y, width, height, &block
      end

      def strike_cells
        width = doc.pdf.bounds.width
        top = 0
        doc.pdf.dash 0.8, space: 2
        doc.pdf.line_width 0.8
        doc.pdf.stroke_color 'B1B1B1'
        @rows.each_with_index do |row, index|
          left = 0
          next_top = top + row.height
          doc.pdf.stroke_vertical_line(
            doc.y(top), doc.y(next_top), at: 0)
          row.columns.each do |width|
            left += width
            doc.pdf.stroke_vertical_line(
              doc.y(top), doc.y(next_top), at: left
            )
          end
          doc.pdf.stroke_horizontal_line 0, width, at: doc.y(0) if index.zero?
          doc.pdf.stroke_horizontal_line 0, width, at: doc.y(next_top)
          top = next_top
        end
        doc.pdf.undash
      end

      def print_content
        top = 0
        @rows.each do |row|
          left = 0
          # line_end_y = top - row.height
          row.columns.each_with_index do |column, index|
            content = row.content[index]
            if content
              position = [
                left, top, column, row.height
              ]
              doc.instance_exec position, &content
            end
            left += column
          end
          top += row.height
        end
      end

      class Row < Struct.new(:height, :columns, :content)
      end

    end

  end

end
