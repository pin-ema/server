# frozen_string_literal: true
require 'prawn'
require 'arabic-letter-connector'
require 'rqrcode'

require_relative '../pdf'

class Server::Pdf::PrawnDoc

  attr_reader :name, :pdf

  def initialize name, **opts
    @name = name
    @pdf = Prawn::Document.new **opts

    pdf.font_families.update({
      'OpenSans' => {
        normal: Rails.root.join('vendor/OpenSans-Regular.ttf'),
        bold: Rails.root.join('vendor/OpenSans-Bold.ttf'),
      },
      'NotoNaskhArabic' => {
        normal: Rails.root.join('vendor/NotoNaskhArabic-Regular.ttf'),
        bold: Rails.root.join('vendor/NotoNaskhArabic-Bold.ttf'),
      },
    })

    pdf.font 'OpenSans'
    pdf.fallback_fonts %w[NotoNaskhArabic]
  end

  def print_axis
    axes = Axes.new pdf
    yield axes if block_given?
    axes.print
  end

  def with_qr_code doc_name, data_string
    png = RQRCode::QRCode.new(
      "#{doc_name}:#{SecureRandom.uuid}:#{data_string}"
    ).as_png(
      module_px_size: 1, border_modules: 0
    )
    temp = Tempfile.new [ "#{name}_qr_code_", '.png' ]
    temp.binmode
    temp.write png.to_s
    temp.flush

    IO.binwrite("/tmp/github-qrcode.png", png.to_s)
    yield temp.path
  end

  def write_into_tempfile
    temp = Tempfile.new "#{name}_pdf_"
    temp.binmode
    temp.write pdf.render
    temp.flush
    temp
  end

  class Axes

    attr_reader :pdf
    attr_accessor :color, :margin, :step_length

    def initialize pdf
      @pdf = pdf
      @color = '000000'
      @margin = 10
      @step_length = 100
    end

    def print
      width = pdf.bounds.width
      height = pdf.bounds.height
      top = height - margin
      right = width - margin

      pdf.save_graphics_state do
        pdf.fill_color color
        pdf.stroke_color color

        print_lines width, height, top, right
        print_horizontal_marks width, top
        print_vertical_marks height, right
      end
    end

    private

    def print_lines width, height, top, right
      pdf.dash 1, space: 4
      pdf.stroke_horizontal_line 0, width, at: margin
      pdf.stroke_horizontal_line 0, width, at: top
      pdf.stroke_vertical_line 0, height, at: margin
      pdf.stroke_vertical_line 0, height, at: right
      pdf.undash
    end

    def print_horizontal_marks width, top
      (step_length..width).step step_length do |point|
        pdf.fill_circle([point, margin], 1)
        pdf.fill_circle([point, top], 1)
        pdf.draw_text(
          point,
          at: [0 + point - 5, top - 10],
          size: 7
        )
      end
    end

    def print_vertical_marks height, right
      (step_length..height).step step_length do |point|
        text = point
        point = height - point
        pdf.fill_circle([margin, 0 + point], 1)
        pdf.fill_circle([right, 0 + point], 1)
        pdf.draw_text(
          text,
          at: [margin + 3, 0 + point - 3],
          size: 7
        )
      end
    end

  end

end
