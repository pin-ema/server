# frozen_string_literal: true

module Server
  module Actions
    extend ActiveSupport::Concern

    included do
      rescue_from StandardError do |exception|
        p "StandardError"
        log_exception exception
        application_fail exception
      end
    end

    private

    def can_user?
      false
    end

    def application_fail exception
      debug = if Rails.env.test?
        {
          error: exception.message,
          error_path: exception.backtrace.first
        }
      end

      render status: :internal_server_error, json: {
        ok: false,
        message: 'server_fail',
        **(debug || {}),
      }
    end

    def authentication_fail
      render status: :unauthorized, json: {
        ok: false,
        message: 'authn_fail',
      }
    end

    def user_missing_fail
      render status: :unauthorized, json: {
        ok: false,
        message: 'user_missing',
      }
    end

    def authorization_fail
      render status: :forbidden, json: {
        ok: false,
        message: 'authr_fail',
      }
    end

    def respond_ok message: nil, **payload
      data = { ok: true }
      data[:message] = message if message
      data[:payload] = payload unless payload.empty?
      render json: data
    end

    def respond_failed reason, status=:ok, **data
      data[:ok] = false
      data[:message] = reason
      render status: status, json: data
    end

    # TODO goes with record v1
    def respond_record_not_found
      respond_ok(
        success: false,
        message: 'records.not_found',
      )
    end

    # TODO goes with record v1
    def yield_param param_name, &block
      params[param_name].presence&.yield_self &block
    end

    # TODO goes with record v1
    def pagination_params per_page_options: nil
      page = params[:page].presence&.to_i || 1

      per_page = params[:per_page].presence&.to_i
      if per_page_options
        per_page = per_page_options.first unless per_page_options.include? per_page
      end

      [page, per_page]
    end

    def log_exception exception, backtrace: true
      backtrace_listing = if backtrace
        Rails.backtrace_cleaner
          .clean(exception.backtrace)
          .join "\n"
      end

      Rails.logger.error(
        [
          "  \033[0;31mFAIL: #{exception.class.name}\033[0m",
          exception.message.presence,
          backtrace_listing,
        ].compact.join "\n"
      )
    end

  end
end
