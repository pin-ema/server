# frozen_string_literal: true

class SchoolYear < ApplicationRecord

  include ApplicationRecord::Nameable

  belongs_to :country

  validate :validate_limits

  before_save do
    self.years = if limits.present?
      limits.map{ _1.year }.uniq.join ' '
    end
  end

  def labels
    {
      caption: name_en,
      name: name_local,
      year_label: year_label,
    }
  end

  def year_label
    if years.present?
      years = self.years.split ' '
      if years.length == 1
        years.first
      else
        "#{years.first}/#{years.last[-2..-1]}"
      end
    end
  end

  def education_levels_ids= list
    self.education_levels = list.map(&:to_i).uniq
  end

  def terms
    return [] if limits.blank?
    limits.each_slice(2).map do |pair|
      date_from, date_to = pair
      { from: date_from, to: date_to }
    end
  end

  def terms= value
    limits = []
    value.each do |term|
      limits.push term[:from]
      limits.push term[:to]
    end
    limits.map! do |item|
      item.is_a?(Date) ? item : (Date.parse item rescue nil)
    end
    self.limits = limits
  end

  private

  def validate_limits
    if limits.present? &&
      limits.length.even? &&
      limits.none?{ not _1.is_a? Date } &&
      limits == limits.sort

      terms_overlap = false
      ((limits.length / 2) - 1).times do |i|
        terms_overlap = limits[i + 1] == limits[i + 2]
      end
      return unless terms_overlap
    end
    errors.add :limits, 'invalid'
  end
end
