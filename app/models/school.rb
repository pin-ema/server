# frozen_string_literal: true

class School < ApplicationRecord

  include ApplicationRecord::Nameable

  belongs_to :country
  belongs_to :director, class_name: 'Person', optional: true

  validates :external_id, uniqueness: { scope: :country_id }, allow_nil: true
  validates :education_levels_ids, presence: true
  validates :education_types, presence: true
  validates :gender_dedications, presence: true
  validates :classrooms_count, numericality: true
  validates :male_latrines_count, numericality: true
  validates :female_latrines_count, numericality: true

  has_many :courses, dependent: :destroy

  before_save do
    self[:external_id] = nil if external_id.blank?
  end

  def labels
    {
      caption: name_en,
      name: name_local,
      external_id: external_id,
    }
  end

  def to_s_address_line
    return '' if address.blank?
    address_system = LocationSystem.get_address_for country
    return '' unless address_system
    locations = Location.where(location_system: address_system, id: address)
    locations.map{ _1.name[0] }.join '; '
  end

end
