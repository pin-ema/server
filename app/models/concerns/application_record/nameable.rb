module ApplicationRecord::Nameable
  # extend ActiveSupport::Concern

  # included do
    # validate :validate_name
  # end

  def name_en
    name&.[](0)
  end

  def name_local
    name&.[](1)
  end

  # private

  # def validate_name
  #   if name.blank? || name_en.blank? || name_local.blank?
  #     errors.add :name, :invalid
  #   end
  # end

end
