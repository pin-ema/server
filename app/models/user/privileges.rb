module User::Privileges

  def self.cast_country_admin **_
    CountryAdmin.new
  end

  def self.cast_collector **_
    Collector.new
  end

end
