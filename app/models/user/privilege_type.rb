class User::PrivilegeType < ActiveRecord::Type::Value

  def cast value
    return [] unless value.is_a? Array
    value.map do |item|
      item = item.to_h if item.is_a? ActionController::Parameters
      next unless item.is_a? Hash

      unless item.is_a? ActiveSupport::HashWithIndifferentAccess
        item = ActiveSupport::HashWithIndifferentAccess.new item
      end
      User::Privileges.try "cast_#{item[:type]}", **item
    end
  end

  def deserialize value
    value = JSON.parse value rescue nil
    cast value
  end

  def serialize value
    list = User::PrivilegeType.map_data value
    list.empty? ? nil : list.to_json
  end

  def self.map_data value
    value.filter_map{ _1&.as_data }
  end

end
