module User::Privileges
  class CountryAdmin

    def as_data
      { type: :country_admin }
    end

  end
end
