module User::Privileges
  class Collector

    def as_data
      { type: :collector }
    end

  end
end
