# frozen_string_literal: true

class Course < ApplicationRecord

  include ApplicationRecord::Nameable

  belongs_to :school
  belongs_to :education_level
  belongs_to :school_year, optional: true
  belongs_to :standardized_course, optional: true

  validates :grade, presence: true
  # validate :validate_time_range

  has_many :groups, dependent: :destroy

  def time_ranges
    return [] if self[:time_ranges].blank?
    self[:time_ranges].each_slice(2).map do |from, to|
      { from: from, to: to }
    end
  end

  def time_ranges= list
    self[:time_ranges] = list
      .map{[_1[:from], _1[:to]]}
      .flatten
      .map{ _1&.to_datetime rescue nil }
  end

  def labels
    {
      caption: name_en,
      name: name_local,
      school: school.name_en,
      education_level: education_level.name_en,
    }
  end

  private

  def validate_time_range
    time_ranges = self[:time_ranges]
    unless time_ranges&.length&.even? && time_ranges.all?(&:present?)
      errors.add :times_ranges
    end
  end

end
