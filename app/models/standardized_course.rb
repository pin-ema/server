# frozen_string_literal: true

class StandardizedCourse < ApplicationRecord

  include ApplicationRecord::Nameable

  belongs_to :education_level

  validates :grade, presence: true

  def labels
    {
      caption: name_en,
      name: name_local,
    }
  end

end
