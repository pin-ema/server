# frozen_string_literal: true
require 'csv'

class TranslationValue < ApplicationRecord
  KEY_SEPARATOR = '.'

  validates :language, presence: true
  validates :key, presence: true

  def key= key
    super key&.strip.presence
  end

  def value= value
    value = value&.strip.presence
    if value && value[0..1] == '->'
      self.link = true
      super value[2..].strip

    else
      self.link = false
      super value

    end
  end

  def self.each_translation_csv_row language, &block
    TranslationValue
      .where(language: language)
      .order(:key)
      .find_in_batches do |batch|
      batch.each do |t|
        block.call "#{t.key},\"#{t.value}\"#{t.link ? ',1' : nil}"
      end
    end
  end

  def self.import_language_from_csv language, csv
    values = CSV.parse(csv).map do |row|
      key, value, link = row
      next if key.blank? or key.match?(/^\s*#/)
      {
        language: language,
        key: key,
        value: value,
        link: link == '1'
      }
    end
    values.compact!

    return if values.empty?
    TranslationValue.transaction do
      scope = TranslationValue.where language: language
      while scope.any?
        scope.limit(1_000).delete_all
      end
      TranslationValue.insert_all! values
      SystemTranslations.create_current! language
    end
  end

end
