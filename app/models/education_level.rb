# frozen_string_literal: true

class EducationLevel < ApplicationRecord

  include ApplicationRecord::Nameable

  belongs_to :country

  validates :level, presence: true

  has_many :standardized_courses, dependent: :destroy

  def labels
    {
      caption: name_en,
      name: name_local,
      level: level,
    }
  end

end
