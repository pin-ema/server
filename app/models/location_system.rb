# frozen_string_literal: true

class LocationSystem < ApplicationRecord

  belongs_to :country
  has_many :locations, dependent: :destroy

  validates :country, presence: true
  validates :label, presence: true
  validates :levels, presence: true

  def self.get_address_for country
    LocationSystem.find_by country: country, label: 'address'
  end

end
