# frozen_string_literal: true

class Person < ApplicationRecord

  belongs_to :country

  validates :born_on, presence: true
  validate :validate_name

  def labels
    {
      caption: full_name_en
    }
  end

  def full_name_en
    "#{first_name[0]} #{last_name[0]}"
  end

  def full_name_arabic
    [ first_name, father_first_name, last_name ]
      .map{ _1[1]&.connect_arabic_letters }
      .join(' ')
  end

  private

  def validate_name
    invalid_name = first_name.blank? || first_name[0].blank? ||
      first_name[1].blank? ||
      last_name.blank? || last_name[0].blank? || last_name[1].blank?
    if invalid_name
      errors.add :first_name, :invalid
    end
  end

end
