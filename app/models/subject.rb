# frozen_string_literal: true

class Subject < ApplicationRecord

  include ApplicationRecord::Nameable

  belongs_to :country

  def labels
    {
      caption: name_en,
      name: name_local,
    }
  end

  def education_levels_ids= list
    self.education_levels = list.map(&:to_i).uniq
  end

  def categories_ids= list
    self.categories = list.map(&:to_i).uniq
  end

end
