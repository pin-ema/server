# frozen_string_literal: true

class Project < ApplicationRecord

  include ApplicationRecord::Nameable

  belongs_to :country

  def labels
    {
      caption: name_en,
      code: code,
    }
  end

end
