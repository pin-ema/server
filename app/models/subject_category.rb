# frozen_string_literal: true

class SubjectCategory < ApplicationRecord

  include ApplicationRecord::Nameable

  belongs_to :country

  def labels
    {
      caption: name_en,
      name: name_local,
    }
  end

end
