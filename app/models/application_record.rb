# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base

  self.abstract_class = true

  def to_b_record
    {
      id: id.to_s,
      **try(:labels),
    }
  end

  def to_id_text
    "[#{id}]"
  end

  def get_errors
    errors.map do |error|
      [
        error.attribute,
        [ error.type, error.options[:message] ].compact.join('.'),
      ]
    end
  end

  def archive= _
    self.archived_at = DateTime.now if respond_to? :archived_at=
  end

end
