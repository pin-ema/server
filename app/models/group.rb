# frozen_string_literal: true

class Group < ApplicationRecord

  belongs_to :course

  validates :term, presence: true

  def labels
    {}
  end

  def scope_students
    Person.where id: students_ids
  end

  def get_associated_project
    Project
      .where(country: course.school.country)
      .where("#{course.school.id} = ANY (projects.schools_ids)")
      .first
  end

end
