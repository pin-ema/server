# frozen_string_literal: true

require 'csv'

class SystemTranslations < ApplicationRecord

  LANGUAGES = %i[en cs pt syr].freeze
  LIMIT_COUNT = 2

  def self.create_current! language
    raise "invalid language #{language}" unless LANGUAGES.include? language.to_sym

    scope = all.where(language: language)

    previous_version = scope.select(:id).first&.id || 0
    timestamp = Time.now.to_i.to_s
    hash = Digest::MD5.hexdigest timestamp + SecureRandom.hex(10)
    new_key = "v#{previous_version + 1}-#{hash}"

    clear_versions scope if scope.count > LIMIT_COUNT

    create!(
      language: language,
      key: new_key,
      payload: prepare_version(language)
    )
  end

  def self.clear_versions scope
    scope = scope.order(created_at: :desc).offset(LIMIT_COUNT)
    scope.limit(1_000).delete_all while scope.any?
  end

  def self.prepare_version language
    scope = TranslationValue.where(language: language)
    tree = KeysTree.new

    scope
      .where(link: false)
      .select(:id, :key, :value)
      .find_each do |translation|
      tree.set translation.key, translation.value
    end

    scope
      .where(link: true)
      .joins(
        "
          JOIN translation_values as values
            ON values.key = translation_values.value
              AND values.language = translation_values.language
        "
      )
      .select(:id, :key, 'values.value as linked_value')
      .find_each do |linked_translation|
      tree.set linked_translation.key, linked_translation.linked_value
    end

    text = tree.to_json
    text << "\n"
    text
  end

  class KeysTree

    def initialize
      @tree = {}
    end

    def to_json *_
      JSON.generate @tree,
        indent: ' ',
        space: ' ',
        object_nl: "\n"
    end

    def get key
      path = key.split('.')
      branch = last_branch_of path
      value = branch[path[-1]]
      value.is_a?(String) ? value : nil
    end

    def set key, value
      path = key.split('.')
      value_name = path[-1]
      branch = last_branch_of path
      branch[value_name] = value
    end

    private

    def last_branch_of path
      case path.length
        when 0 then raise 'bad key'
        when 1 then @tree
        else traverse! path
      end
    end

    def traverse! path
      path.pop
      branch = @tree

      path.each do |branch_name|
        next_branch = branch[branch_name]
        branch = if next_branch.is_a? Hash
          next_branch
        else
          branch[branch_name] = {}
        end
      end

      branch
    end

  end

end
