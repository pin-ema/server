module Country::Defaults
  # extend ActiveSupport::Concern

  def fill_defaults!
    seld.class.transaction do
      levels = EducationLevel.where(country: self)
      levels.create!({ name: [ '0 EARLY CHILDHOOD EDUCATION' ], level: 0 })
      levels.create!({ name: [ '1 PRIMARY EDUCATION' ], level: 1 })
      levels.create!({ name: [ '2 LOWER SECONDARY EDUCATION' ], level: 2 })
      levels.create!({ name: [ '3 UPPER SECONDARY EDUCATION' ], level: 3 })
      levels.create!({ name: [ '4 POST-SECONDARY NON-TERTIARY EDUCATION' ], level: 4 })
      levels.create!({ name: [ '5 SHORT-CYCLE TERTIARY EDUCATION' ], level: 5 })
      levels.create!({ name: [ '6 BACHELOR\'S OR EQUIVALENT LEVEL' ], level: 6 })
      levels.create!({ name: [ '7 MASTER\'S OR EQUIVALENT LEVEL' ], level: 7 })
      levels.create!({ name: [ '8 DOCTORAL OR EQUIVALENT LEVEL' ], level: 8 })
      levels.create!({ name: [ '9 NOT ELSEWHERE CLASSIFIED' ], level: 9 })
    end
  end

end
