# frozen_string_literal: true
require 'securerandom'

class User < ApplicationRecord
  TMP_INVALID_PASSWORD = '-1'

  belongs_to :country, optional: true

  attribute :privileges, PrivilegeType.new

  validates :login, presence: true
  validates_uniqueness_of :login, { scope: :country }
  validates_length_of :login, { minimum: 3 }
  validate :validate_password
  validate :validate_privileges

  validates_presence_of :full_name_en

  def admissible_action? scope, action
    return true if is_root?


    true # TODO big time
    # case scope
    #   when 'users', 'standardized_courses', 'education_levels',
    #     'subjects', 'subject_categories', 'school_years'
    #     return privileges.any?{ _1.is_a? User::Privileges::CountryAdmin }
    #   when 'courses', 'groups', 'people', 'schools', 'server/pdf',
    #     'location_systems', 'locations'
    #     return true
    # end
    #
    # false
  end

  def password= value
    self.password_digest = TMP_INVALID_PASSWORD
    self.password_digest = BCrypt::Password.create(value) if valid_password? value
  end

  def full_name= names
    names = [nil, nil] unless names.is_a? Array
    write_attribute :full_name_en, names[0]
    write_attribute :full_name, names[1]
  end

  def password_matches? value
    return false if password_digest.nil?

    ::BCrypt::Password.new(password_digest) == value
  end

  def lock_account wipe_password=false
    self.lock = '1'
    self.locked_at = Time.now
    self.password_digest = nil if wipe_password
  end

  def unlock_account
    self.lock = nil
  end

  def is_privileged? type
    privileges.any?{ _1.as_data[:type] == type }
  end

  private

  def validate_password
    errors.add :password, :invalid if password_digest == TMP_INVALID_PASSWORD
  end

  def valid_password? value
    return false if value.blank? || value.length < 6 || value.length > 100

    [/\d/, /[a-z]/, /[A-Z]/].map{ value.index _1 }.uniq.length == 3
  end

  def validate_privileges
    errors.add :privileges, :invalid if privileges.include? nil
  end

end
