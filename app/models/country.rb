# frozen_string_literal: true

class Country < ApplicationRecord

  include ApplicationRecord::Nameable
  include Country::Defaults

  has_many :material_kits, dependent: :destroy
  has_many :work_agreements, dependent: :destroy

  has_many :schools, dependent: :destroy
  has_many :school_years, dependent: :destroy
  has_many :education_levels, dependent: :destroy

  has_many :projects, dependent: :destroy

  has_many :location_systems, dependent: :destroy
  has_many :people, dependent: :destroy
  has_many :donors, dependent: :destroy
  has_many :subject_categories, dependent: :destroy
  has_many :subjects, dependent: :destroy

  has_many :users, dependent: :destroy

  def labels
    {
      caption: name_en,
      name: name_local,
    }
  end

  def self.public_list
    all.to_a.sort_by{ _1.name_en.to_s }.map do |country|
      Web::Records.map_associated_record country
    end
  end

end
