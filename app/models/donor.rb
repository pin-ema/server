class Donor < ApplicationRecord

  include ApplicationRecord::Nameable

  belongs_to :country

  validates :code, presence: true

  def labels
    {
      caption: name_en,
      code: code,
    }
  end

end
