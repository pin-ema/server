# frozen_string_literal: true

class GroupSchedule < ApplicationRecord
  belongs_to :group

  def subjects_from_occurrences= list
    index = {}
    list.each do |id, date|
      subject = (index[id] ||= [])
      subject.push date
    end
    self.subjects = index
  end

  def occurrences_list
    list = []
    subjects&.each do |subject_id, dates|
      list += dates.map{ [ subject_id, _1 ] }
    end
    list
  end

end
