# frozen_string_literal: true

class TranslationTemplate < ApplicationRecord
  def file= io
    self.csv = io.read
  end
end
