# frozen_string_literal: true

class Location < ApplicationRecord

  belongs_to :location_system

end
