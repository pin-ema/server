# frozen_string_literal: true

class Classification < ApplicationRecord

  belongs_to :group

end
