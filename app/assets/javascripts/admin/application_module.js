function error(...args) {
    console.error(...args);
}

export function run(callback) {
    if (callback === undefined) callback = () => {
        actions.attachToButtons(document.body, actions.index);
    };

    document.addEventListener("DOMContentLoaded", callback);
}

export function find(selector) {
    return document.querySelector(selector);
}

export function findAll(selector) {
    return Array.from(document.querySelectorAll(selector));
}

export function fillContainer(container, html) {
    const range = document.createRange();
    container.innerHTML = '';
    container.appendChild(range.createContextualFragment(html));
}

export async function delay (time, callback) {
    await new Promise(resolve => setTimeout(resolve, time));
    callback();
}

export const actions = {
    index: {},
    add (name, fn) {
        actions.index[name] = fn;
    },
    attachToButtons (container, actions) {
        function action(event) {
            let action = this.dataset.action;
            action = action && actions[action];
            if (action) {
                event.preventDefault();
                action.call(this, event);
            }
        }
        container.querySelectorAll('button[data-action]').forEach(button => {
            button.addEventListener('click', action);
        });
    },
    lockButton (button) {
        button.disabled = true;
        const spinner = button.querySelector('.spinner-border');
        spinner.classList.remove('d-none');
        return () => {
            button.disabled = false;
            spinner.classList.add('d-none');
        };
    },
    findButtonByAction (actionName, container=document.body) {
        container.querySelector(`button[data-action="${actionName}"]`);
    },
};
