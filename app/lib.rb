def niy! msg=nil
  raise "not implemented yet#{ " - #{msg}" }"
end

def dbg! binding
  if Rails.env.production?
    # here, emoji
    niy! '♦️#dbg! is disabled in production (for now)'
  end
  require 'irb'
  binding.irb
  exit 1
end

def log_object object
  puts '======='
  puts object.class
  puts object
  puts object.to_s
  puts '======='
end
