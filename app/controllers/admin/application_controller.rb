# frozen_string_literal: true

module Admin
  class ApplicationController < ActionController::Base
    # left in credentials Rails.application.credentials.dig :admin, :simple_password

    include Server::Actions
    include Server::CurrentUser

    before_action :authenticate!
    before_action :authorize!

    private

    def authorize!
      return if current_user.is_root?

      is_admin = current_user.privileges.any?{ _1.is_a? User::Privileges::CountryAdmin }
      authorization_fail unless is_admin
    end

  end
end
