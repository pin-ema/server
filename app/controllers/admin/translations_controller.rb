# frozen_string_literal: true

module Admin
  class TranslationsController < ApplicationController

    def edit_language
      csv_lines = []
      TranslationValue.each_translation_csv_row params[:language] do |next_value|
        csv_lines.push next_value
      end
      @language = csv_lines.join "\n"
    end

    def update_language
      language = params[:language]
      csv = params[:csv]&.strip || ''
      TranslationTemplate.create! language: language, csv: csv
      TranslationValue.import_language_from_csv language, csv
      on_translation_changed language
    end

    private

    def on_translation_changed language
      SystemTranslations.create_current! language
      if Rails.env.development?
        path = Rails.root.join "db/translations/#{language}.csv"
        File.open path, 'w' do |f|
          TranslationValue.each_translation_csv_row language do |next_value|
            f.puts next_value
          end
        end
      end
    end

  end
end
