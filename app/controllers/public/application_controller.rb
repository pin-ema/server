# frozen_string_literal: true

module Public
  class ApplicationController < ActionController::Base

    include Server::Actions

    def countries
      respond_ok list: Country.public_list
    end

  end
end
