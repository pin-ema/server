# frozen_string_literal: true

module Server
  class ApplicationController < ActionController::Base

    include Server::Actions
    include Server::CurrentUser

    before_action :authenticate!
    before_action :authorize!

    private

    def authorize!
      unless current_user.admissible_action? "server/#{controller_name}", action_name
        authorization_fail
      end
    end

  end
end
