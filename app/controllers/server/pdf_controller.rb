# frozen_string_literal: true

module Server
  class PdfController < ApplicationController

    def group_attendance
      group = Group.find params[:group_id]
      pdf = Server::Pdf::Documents::GroupAttendance.new group
      pdf.generate
      pdf.prawn_doc.print_axis if params[:debug] == '1'
      temp = pdf.prawn_doc.write_into_tempfile
      send_file temp,
        type: 'application/pdf',
        disposition: 'inline',
        filename: "attendance_group_#{group.id}.pdf"
    end

  end
end
