# frozen_string_literal: true

module Web
  class ProjectsController < ApplicationController

    include Records
    include CountryAssociation
    include CountryBoundary
    add_irregular_association :school

    def index
      scope = records_scope

      yield_param(:search){
        scope = scope.where Web.name_like_sql(Project, _1)
      }

      search_resp scope
    end

    def show
      record_get_resp
    end

    def create
      record_create create_params
      record_creation_resp
    end

    def update
      record_get&.update update_params
      record_update_resp
    end

    private

    def map_record project
      super.update(
        code: project.code,
        name: project.name,
        starts_on: project.starts_on,
        ends_on: project.ends_on,
        donors: project.donors,
        schools_ids: project.schools_ids,
      )
    end

    def irregular_association_school projects
      return if projects.empty?

      ids = projects.map{ _1.schools_ids || [] }.flatten.compact.uniq
      School.where country_id: current_user_countries.map(&:id), id: ids
    end

    def record_params
      params[:project]&.permit :country_id,
        :code, :starts_on, :ends_on, :donors,
        name: [], schools_ids: []
    end

  end
end
