# frozen_string_literal: true

module Web
  class ApplicationController < ActionController::API
    wrap_parameters false

    include Server::Actions
    include Server::CurrentUser

    before_action :authenticate!
    before_action :authorize!

    private

    def authorize!
      unless current_user.admissible_action? controller_name, action_name
        authorization_fail
      end
    end

  end
end
