# frozen_string_literal: true

module Web
  class PeopleController < ApplicationController

    include Records
    include CountryAssociation
    include CountryBoundary

    def index
      table = Person.arel_table
      groupsTable = Group.arel_table
      scope = records_scope

      yield_param(:search){
        scope = scope.where(
          [
            Web.name_like_sql(Person, _1, attribute: :first_name),
            Web.name_like_sql(Person, _1, attribute: :last_name),
          ].
            map{|q| "(#{q})"}.join(' OR ')
        )
      }

      if (param = params[:group_id])
        scope = scope.
          joins(table.join(groupsTable).on(
            "groups.id = #{param} AND people.id = ANY (groups.students_ids)"
          ).join_sources)
      end

      yield_param(:kobo_id){
        scope = scope.where student_kobo_no: _1
      }

      search_resp scope
    end

    def show
      record_get_resp
    end

    def create
      record_create create_params
      record_creation_resp
    end

    def update
      record_get&.update update_params
      record_update_resp
    end

    private

    def map_record person
      record = super
      %i[
      first_name last_name disabilities outside_school nationality
      caregivers
       born_on gender address
          citizen_id
          passport_no telephone_no mother_tongue spoken_languages
          enrollment_reasons registered_on
          disability_diagnosis assistance_needed assistance_provided
          disability_note
          residency_status school_distance_km school_distance_min
          school_transport
      ].each{ record[_1] = person.send _1 }
      record
    end

    def record_params
      params[:person]&.permit(
        %i[
        country_id
          born_on gender
          citizen_id
          passport_no telephone_no mother_tongue
          enrollment_reasons registered_on outside_school nationality
          disability_diagnosis assistance_needed assistance_provided
          disability_note
          school_distance_km school_distance_min
        ],
        first_name: [], last_name: [], disabilities: [], spoken_languages: [],
        residency_status: [], school_transport: [],
        address: [],
        caregivers: [
          :relation, :citizen_id, :phone_no, :email,
          :cash_for_work, :humansis_cwf_id,
          { first_name: [], last_name: [] }
        ]
      )
    end

  end
end
