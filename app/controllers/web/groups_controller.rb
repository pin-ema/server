# frozen_string_literal: true

module Web
  class GroupsController < ApplicationController

    include Records
    include CountryBoundary
    add_association :course
    add_irregular_association :school

    def index
      table = Group.arel_table
      scope = records_scope

      # if (param = params[:search].presence)
      #   scope = scope.where table[:name_en].matches("%#{param}%")
      # end

      if (param = params[:course_id].presence)
        scope = scope.where course_id: param
      end

      search_resp scope
    end

    def show
      record_get_resp
    end

    def create
      record_create create_params
      record_creation_resp
    end

    def update
      record_get&.update update_params
      record_update_resp
    end

    def show_schedule
      return :internal_server_error
      course = record_get.course
      schedule = GroupSchedule.find_by group: record_get
      respond_ok(
        subjects: course.subjects_scope.map{
          Web::Records.map_associated_record _1
        },
        settings: course.subjects_settings,
        occurrences: schedule&.occurrences_list || []
      )
    end

    def update_schedule
      record_get
      schedule = GroupSchedule.find_by group: record_get
      schedule ||= GroupSchedule.new group: record_get
      result = schedule.update subjects_from_occurrences: params[:occurrences]
      record_change_resp result
    end

    private

    def records_scope_base
      super.
        includes(course: :school)
    end

    def records_scope_country_filter scope
      scope.where(course: { schools: { country_id: requested_country_ids } })
    end

    def map_record group
      super.update(
        course_id: group.course.id,
        school_id: group.course.school_id,
        name: group.name,
        term: group.term,
      )
    end

    def record_params
      params[:group]&.permit(%i[
        course_id
        term
      ], name: [])
    end

    def irregular_association_school groups
      return if groups.empty?

      ids = groups.map{ _1.course.school_id }.uniq
      School.where country_id: current_user_countries.map(&:id), id: ids
    end

  end
end
