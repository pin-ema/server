# frozen_string_literal: true

module Web
  class LocationSystemsController < ApplicationController

    include Records
    include CountryBoundary

    def index
      scope = records_scope
      search_resp scope
    end

    private

    def records_scope
      scope = super
      yield_param(:label){ scope = scope.where label: _1 }
      scope
    end

    def map_record location_system
      super.update(
        name: location_system.name,
        levels: location_system.levels,
        settings: location_system.settings,
      )
    end

  end
end
