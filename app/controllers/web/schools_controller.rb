# frozen_string_literal: true

module Web
  class SchoolsController < ApplicationController

    include Records
    include CountryAssociation
    include CountryBoundary
    add_association :director
    add_irregular_association :education_level

    def index
      table = School.arel_table
      scope = records_scope

      yield_param(:search){
        scope = scope.where(
          [
            Web.name_like_sql(School, _1),
            table[:external_id].matches("%#{_1}%"),
          ].
            reduce(:or)
        )
      }

      yield_param(:id){
        scope = scope.where(id: _1)
      }

      search_resp scope
    end

    def show
      record_get_resp
    end

    def create
      record_create create_params
      record_creation_resp
    end

    def update
      record_get&.update update_params
      record_update_resp
    end

    def archive
      record_get&.update archived: true
      record_update_resp
    end

    private

    def records_scope_base
      super.where archived: nil
    end

    def map_record school
      super.update(
        education_levels_ids: school.education_levels_ids,
        name: school.name,
        external_id: school.external_id,
        education_types: school.education_types,
        director_id: school.director&.id,
        gender_dedications: school.gender_dedications,
        classrooms_count: school.classrooms_count,
        male_latrines_count: school.male_latrines_count,
        female_latrines_count: school.female_latrines_count,
        address: school.address,
      )
    end

    def irregular_association_education_level schools
      return if schools.empty?

      ids = schools.map{ _1.education_levels_ids || [] }.flatten.compact.uniq
      EducationLevel.where country_id: current_user_countries.map(&:id), id: ids
    end

    def record_params
      params[:school]&.permit(
        %i[
          country_id external_id director_id
           classrooms_count male_latrines_count
          female_latrines_count
        ],
        name: [], education_levels_ids: [],
        address: [],
        education_types: [], gender_dedications: []
      )
    end

  end
end
