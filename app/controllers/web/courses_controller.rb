# frozen_string_literal: true

module Web
  class CoursesController < ApplicationController

    include Records
    include CountryBoundary
    add_association :school
    add_association :education_level
    add_association :school_year
    add_association :standardized_course
    add_irregular_association :subject
    add_irregular_association :teacher

    def index
      scope = records_scope

      yield_param(:search){
        scope = scope.where Web.name_like_sql(Course, _1)
      }

      yield_param(:school_id){
        scope = scope.where education_level_id: _1
      }

      yield_param(:education_level_id){
        scope = scope.where education_level_id: _1
      }

      search_resp scope
    end

    def show
      record_get_resp
    end

    def create
      record_create create_params
      record_creation_resp
    end

    def update
      record_get&.update update_params
      record_update_resp
    end

    private

    def records_scope_base
      super.
        includes(:school, :education_level, :school_year, :standardized_course)
    end

    def records_scope_country_filter scope
      scope.where(education_level: { country_id: requested_country_ids })
    end

    def map_record course
      super.update(
        school_id: course.school.id,
        education_level_id: course.education_level.id,
        school_year_id: course.school_year&.id,
        standardized_course_id: course.standardized_course&.id,
        name: course.name,
        grade: course.grade,
        is_formal: course.is_formal,
        accreditation_authority: course.accreditation_authority,
        lesson_duration: course.lesson_duration,
        attendance_limit: course.attendance_limit,
        preferred_grading: course.preferred_grading,
        time_ranges: course.time_ranges,
        description: course.description,
        subjects: course.subjects,
      )
    end

    def record_params
      course = params.require :course
      course.permit(
        %i[ school_id education_level_id school_year_id standardized_course_id
        grade is_formal lesson_duration
        attendance_limit description],
        name: [], accreditation_authority: [], preferred_grading: [],
        time_ranges: [:from, :to],
        subjects: [:subject_id, :teacher_id, :exam, :per_week, grading: []]
      )
    end

    def irregular_association_subject courses
      return if courses.empty?

      ids = courses.map{ _1.subjects || [] }
        .flatten.compact
        .map{ _1['subject_id'] }
        .compact.uniq
      Subject.where country_id: current_user_countries.map(&:id), id: ids
    end

    def irregular_association_teacher courses
      # TODO fix
      return
      # return if courses.empty?
      #
      # ids = courses.map{ _1.subjects || [] }
      #   .flatten.compact
      #   .map{ _1['teacher_id'] }
      #   .compact.uniq
      # Person.where country_id: current_user_countries.map(&:id), id: ids
    end

  end
end
