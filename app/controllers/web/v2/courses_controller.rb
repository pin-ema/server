class Web::V2::CoursesController < Web::V2::ApplicationController
  include Web::V2::Recordable

  include Web::V2::Recordable::IndexAction
  include Web::V2::Recordable::CreateAction
  include Web::V2::Recordable::UpdateAction

  private

  def records_scope
    super
      .includes(:school, :education_level, :school_year, :standardized_course)
      .where(education_level: { country: @records_scope_countries })
  end

  def index_filtered_scope scope
    if (param = params[:search].presence)
      scope = scope.where Web.name_like_sql(Course, param)
    end

    # if (param = params[:school_id].presence)
    #   scope = scope.where school_id: param
    # end
    #
    # if (param = params[:education_level_id].presence)
    #   scope = scope.where education_level_id: param
    # end

    scope
  end

  def map_a_record record
    super.update(
      school_id: record.school.id,
      education_level_id: record.education_level.id,
      school_year_id: record.school_year&.id,
      standardized_course_id: record.standardized_course&.id,
      name: record.name,
      grade: record.grade,
      is_formal: record.is_formal,
      accreditation_authority: record.accreditation_authority,
      lesson_duration: record.lesson_duration,
      attendance_limit: record.attendance_limit,
      preferred_grading: record.preferred_grading,
      time_ranges: record.time_ranges,
      description: record.description,
      subjects: record.subjects,
    )
  end

  def records_associations records
    {
      schools: records.map{ _1.school }.uniq,
      education_levels: records.map{ _1.education_level }.uniq,
      school_years: records.map{ _1.school_year }.uniq,
      standardized_courses: records.map{ _1.standardized_course }.uniq,
      subject: begin
        ids = records
          .map{ _1.subjects || [] }.flatten.compact
          .map{ _1['subject_id'] }.compact.uniq
        Subject.where country: @records_scope_countries, id: ids
      end,
    }
  end

  def record_params
    course = params.require :course
    course.permit(
      %i[ school_id education_level_id school_year_id standardized_course_id
        grade is_formal lesson_duration
        attendance_limit description],
      name: [], accreditation_authority: [], preferred_grading: [],
      time_ranges: [:from, :to],
      subjects: [:subject_id, :exam, :per_week, grading: []]
    )
  end

end
