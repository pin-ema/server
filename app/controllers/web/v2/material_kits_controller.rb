class Web::V2::MaterialKitsController < Web::V2::ApplicationController
  include Web::V2::Recordable

  include Web::V2::Recordable::IndexAction
  include Web::V2::Recordable::CreateAction
  include Web::V2::Recordable::UpdateAction

  private

  def can_user?
    case action_name.to_sym
      when :index, :create, :update
        current_user.is_privileged? :country_admin
    end
  end

  def records_scope
    super
      .includes(:country)
      .where(country: @records_scope_countries)
  end

  def index_filtered_scope scope
    if (param = params[:search].presence)
      scope = scope.where Web.name_like_sql(MaterialKit, param)
    end

    scope
  end

  def map_a_record record
    super.update(
      country_id: record.country.id,
      name: record.name,
      code: record.code,
      contents: record.contents,
    )
  end

  def records_associations records
    {
      countries: records.map(&:country).uniq,
    }
  end

  def record_sanitized_params
    params[:record]&.permit :country_id, :code, :contents,
      name: []
  end

end
