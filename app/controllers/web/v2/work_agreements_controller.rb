class Web::V2::WorkAgreementsController < Web::V2::ApplicationController
  include Web::V2::Recordable

  include Web::V2::Recordable::IndexAction
  include Web::V2::Recordable::CreateAction
  include Web::V2::Recordable::UpdateAction

  private

  def can_user?
    case action_name.to_sym
      when :index, :create, :update
        current_user.is_privileged? :country_admin
    end
  end

  def records_scope
    super
      .includes(:country, :person)
      .where(country: @records_scope_countries)
  end

  def index_filtered_scope scope
    if (param = params[:search].presence)
      scope = scope.where(
        WorkAgreement.arel_table[:code].matches("%#{param}%")
      )
    end

    scope
  end

  def map_a_record record
    super.update(
      country_id: record.country.id.to_s,
      person_id: record.person.id.to_s,
      projects_ids: record.projects_ids&.map(&:to_s),
      donors_ids: record.donors_ids&.map(&:to_s),
      position: record.position,
      starts_on: record.starts_on,
      ends_on: record.ends_on,
      resigned_on: record.resigned_on,
      comment: record.comment,
      created_at: record.created_at,
      updated_at: record.updated_at,
      archived_at: record.archived_at,
    )
  end

  def records_associations records
    {
      countries: records.map(&:country).uniq,
      people: records.map(&:person).uniq,
      projects: Project
        .where(country: @records_scope_countries)
        .where(id: records.map(&:projects_ids).flatten.uniq.compact)
        .to_a,
      donors: Donor
        .where(country: @records_scope_countries)
        .where(id: records.map(&:donors_ids).flatten.uniq.compact)
        .to_a,
    }
  end

  def record_sanitized_params
    params[:record]&.permit :country_id, :person_id, :position,
      :starts_on, :ends_on, :resigned_on, :comment,
      projects_ids: [], donors_ids: []
  end

end
