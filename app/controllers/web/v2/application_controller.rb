class Web::V2::ApplicationController < ActionController::API

  include Server::Actions

  before_action :cookie_authenticate!
  before_action :authorize!

  rescue_from Web::V2::UserNotPresentError do
    authentication_fail
  end

  rescue_from Web::V2::UserNotAuthorizedError do
    authorization_fail
  end

  def current_user
    @current_user or raise Web::V2::UserNotPresentError
  end

  def all_user_countries
    @all_user_countries ||= if !current_user
      []
    elsif current_user.is_root?
      Country.all.to_a
    else
      [ current_user.country ].compact
    end
  end

  def current_country
    if (param = params[:country_id].presence&.to_i)
      all_user_countries.find{ param == _1.id }
    else
      all_user_countries.first
    end
  end

  private

  def cookie_authenticate!
    @current_user = request.env['warden'].authenticate! :password
  end

  def authorize!
    raise Web::V2::UserNotAuthorizedError unless can_user?
  end

end
