class Web::V2::UsersController < Web::V2::ApplicationController
  include Web::V2::Recordable

  include Web::V2::Recordable::IndexAction
  include Web::V2::Recordable::CreateAction
  include Web::V2::Recordable::UpdateAction

  def change_password
    change_record do |record|
      record.update password: params[:password]
    end
  end

  def lock
    change_record do |record|
      if params[:lock]
        record.lock_account params[:wipe_password]
      else
        record.unlock_account
      end
      record.save
    end
  end

  private

  def can_user?
    case action_name.to_sym
      when :index, :create, :update, :change_password, :lock
        current_user.is_privileged? :country_admin
    end
  end

  def records_scope
    super
      .includes(:country)
      .where(country: @records_scope_countries)
  end

  def index_filtered_scope scope
    if (param = params[:search].presence)
      scope = scope.where(
        User.arel_table[:full_name_en].matches("%#{param}%")
      )
    end

    scope
  end

  def map_a_record record
    super.update(
      country_id: record.country.id,
      login: record.login,
      full_name: [record.full_name_en, record.full_name],
      lock: record.lock,
      last_authn: record.last_authn,
      privileges: User::PrivilegeType.map_data(record.privileges),
      is_root: record.is_root?,
    )
  end

  def records_associations records
    {
      countries: records.map(&:country).uniq
    }
  end

  def record_sanitized_params
    permit = []
    permit.push :country_id, :login, full_name: []
    permit.push :is_root
    # TODO simply replace it?
    privileges = params.dig(:user, :privileges)&.try :map, &:permit!
    params[:user]&.delete :privileges
    {
      **params[:user]&.permit(permit),
      privileges: privileges,
    }
  end

end
