# frozen_string_literal: true

module Web
  class SubjectCategoriesController < ApplicationController

    include Records
    include CountryAssociation
    include CountryBoundary

    def index
      scope = records_scope

      yield_param(:search){
        scope = scope.where Web.name_like_sql(SubjectCategory, _1)
      }

      search_resp scope
    end

    def show
      record_get_resp
    end

    def create
      record_create create_params
      record_creation_resp
    end

    def update
      record_get&.update update_params
      record_update_resp
    end

    private

    def map_record subject
      super.update(
        name: subject.name,
      )
    end

    def record_params
      params.require(:subject_category).permit :country_id, name: []
    end

  end
end
