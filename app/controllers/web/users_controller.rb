# frozen_string_literal: true

module Web
  class UsersController < ApplicationController

    include Records
    include CountryAssociation
    include CountryBoundary

    def index
      table = User.arel_table
      scope = records_scope

      if (param = params[:search].presence)
        scope = scope.where table[:full_name_en].matches("%#{param}%")
      end

      search_resp scope
    end

    def show
      record_get_resp
    end

    def create
      record_create create_params
      record_creation_resp
    end

    def update
      record_get&.update update_params
      record_update_resp
    end

    def delete
      result = record_get&.destroy
      respond_ok success: !!result
    end

    def change_password
      record_get&.update password: params[:password]
      record_update_resp
    end

    def lock
      record_get or (return respond_ok success: false)
      if params[:lock]
        record_get.lock_account params[:wipe_password]
      else
        record_get.unlock_account
      end
      respond_ok success: record_get.save
    end

    private

    def record_scope_base
      super.
        where(country_id: current_user_countries)
    end

    def records_scope
      super.
        order :login
    end

    def map_record user
      super.update(
        login: user.login,
        full_name: [user.full_name_en, user.full_name],
        lock: user.lock,
        last_authn: user.last_authn,
        privileges: User::PrivilegeType.map_data(user.privileges),
        is_root: user.is_root?,
      )
    end

    def record_params
      permit = []
      permit.push :country_id, :login, full_name: []
      permit.push :is_root
      # TODO simply replace it?
      privileges = params.dig(:user, :privileges)&.try :map, &:permit!
      params[:user]&.delete :privileges
      {
        **params[:user]&.permit(permit),
        privileges: privileges,
      }
    end

  end
end
