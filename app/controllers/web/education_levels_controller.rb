# frozen_string_literal: true

module Web
  class EducationLevelsController < ApplicationController

    include Records
    include CountryAssociation
    include CountryBoundary

    def index
      scope = records_scope

      yield_param(:search){
        scope = scope.where Web.name_like_sql(EducationLevel, _1)
      }

      search_resp scope
    end

    def show
      record_get_resp
    end

    def create
      record_create create_params
      record_creation_resp
    end

    def update
      record_get&.update update_params
      record_update_resp
    end

    private

    def map_record education_level
      super.update(
          name: education_level.name,
          level: education_level.level,
      )
    end

    def record_params
      params[:education_level]&.permit :country_id, :level, name: []
    end

  end
end
