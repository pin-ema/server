# frozen_string_literal: true

module Web
  class MaterialKitsController < ApplicationController

    include Records
    include CountryAssociation
    include CountryBoundary

    def index
      scope = records_scope

      yield_param(:search){
        scope = scope.where(
          Web.name_like_sql(MaterialKit, _1)
        )
      }

      scope = scope.where archived_at: nil

      search_resp scope
    end

    def show
      record_get_resp
    end

    def create
      record_create create_params
      record_creation_resp
    end

    def update
      record_get&.update update_params
      record_update_resp
    end

    def archive
      record_get&.update! archived_at: DateTime.now
    end

    private

    def map_record material_kit
      super.update(
        name: material_kit.name,
        code: material_kit.code,
        contents: material_kit.contents,
      )
    end

    def record_params
      params[:material_kit]&.permit :country_id, :code, :contents,
        name: []
    end

  end
end
