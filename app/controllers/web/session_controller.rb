# frozen_string_literal: true

module Web
  class SessionController < ApplicationController

    skip_before_action :authenticate!
    skip_before_action :authorize!

    def show
      authenticate!
      respond_ok user: session_user
    end

    def login
      reset_session
      @current_user = request.env['warden'].authenticate :password
      if @current_user
        respond_ok
      else
        respond_failed nil, :unprocessable_entity
      end
    end

    def logout
      request.env['warden'].logout
      respond_ok
    end

    def translations
      language = params[:language]
      version_name = params[:version].presence
      scope = SystemTranslations
        .where(language: language)
        .order(created_at: :desc)

      version = version_name && scope.where(key: version_name).first
      if version
        expires_in 24.hours, public: true if Rails.env.production?
        render plain: version.payload
        return
      end

      unless version_name
        last_version = scope.first
        if last_version
          redirect_to api_web_session_translations_path(version: last_version.key)
          return
        end
      end

      local_version = SystemTranslations.create_current! language
      redirect_to api_web_session_translations_path(version: local_version.key)
    end

    private

    def session_user
      {
        id: current_user.id,
        login: current_user.login,
        name: [current_user.full_name_en, current_user.full_name],
        countries: current_user_countries.map{
          Records.map_associated_record _1
        },
      }
    end

  end
end
