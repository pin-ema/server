# frozen_string_literal: true

module Web
  class LocationsController < ApplicationController

    include Records

    def index
      limit = 50
      scope = records_scope.limit limit

      records = scope.to_a
        .sort_by!{ _1.name[0] }
        .map!{ map_record _1 }
      respond_ok records: records, **Web.build_pagination(
        1, records.length, limit
      )
    end

    private

    def records_scope
      scope = super
      param = params[:location_system_id].presence
      system = param && LocationSystem
        .where(country: current_user_countries)
        .find_by(id: param)

      unless system && current_user_countries.map(&:id).include?(system.country_id)
        return scope.none
      end
      scope = scope.where location_system_id: system.id

      if (param = params[:ids].presence)
        param = param[0..9]
        scope = scope.where id: param

      else
        if (param = params[:parent].presence)
          scope = scope.where parent_id: param
        else
          scope = scope.where level: 1
        end

      end

      scope
    end

    def map_record location
      super.update(
        name: location.name,
        level: location.level,
        parent_id: location.parent_id,
        label: location.label,
      )
    end

  end
end
