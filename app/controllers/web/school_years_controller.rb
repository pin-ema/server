# frozen_string_literal: true

module Web
  class SchoolYearsController < ApplicationController

    include Records
    include CountryAssociation
    include CountryBoundary
    add_irregular_association :education_level

    def index
      table = SchoolYear.arel_table
      scope = records_scope

      yield_param(:search){
        scope = scope.where(
          [
            Web.name_like_sql(SchoolYear, _1),
            table[:years].matches("%#{_1}%"),
          ].
            reduce(:or)
        )
      }

      search_resp scope
    end

    def show
      record_get_resp
    end

    def create
      record_create create_params
      record_creation_resp
    end

    def update
      record_get&.update update_params
      record_update_resp
    end

    def archive
      record_get&.update! archived: true
    end

    private

    def map_record school_year
      super.update(
        education_levels: school_year.education_levels.presence || [],
        year_label: school_year.year_label,
        name: school_year.name,
        terms: school_year.terms,
      )
    end

    def irregular_association_education_level school_years
      return if school_years.empty?

      ids = school_years.map{ _1.education_levels || [] }.flatten.compact.uniq
      EducationLevel.where country_id: current_user_countries.map(&:id), id: ids
    end

    def record_params
      params[:school_year]&.permit :country_id, education_levels_ids: [],
        name: [], terms: %i[from to]
    end

  end
end
