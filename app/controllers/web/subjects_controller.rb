# frozen_string_literal: true

module Web
  class SubjectsController < ApplicationController

    include Records
    include CountryAssociation
    include CountryBoundary
    add_irregular_association :education_level
    add_irregular_association :subject_category

    def index
      scope = records_scope

      yield_param(:search){
        scope = scope.where Web.name_like_sql(Subject, _1)
      }

      if (param = params[:education_level_id])
        scope = scope.where(education_levels: param)
      end

      search_resp scope
    end

    def show
      record_get_resp
    end

    def create
      record_create create_params
      record_creation_resp
    end

    def update
      record_get&.update update_params
      record_update_resp
    end

    private

    def map_record subject
      super.update(
        name: subject.name,
        education_levels: subject.education_levels.presence || [],
        categories: subject.categories.presence || [],
      )
    end

    def irregular_association_education_level subjects
      return if subjects.empty?

      ids = subjects.map{ _1.education_levels || [] }.flatten.compact.uniq
      EducationLevel.where country_id: current_user_countries.map(&:id), id: ids
    end

    def irregular_association_subject_category subjects
      return if subjects.empty?

      ids = subjects.map{ _1.categories || [] }.flatten.compact.uniq
      SubjectCategory.where country_id: current_user_countries.map(&:id), id: ids
    end

    def record_params
      params[:subject]&.permit :country_id, name: [],
        education_levels_ids: [], categories_ids: []
    end

  end
end
