# frozen_string_literal: true

module Web
  class StandardizedCoursesController < ApplicationController

    include Records
    include CountryBoundary
    add_association :education_level
    add_irregular_association :subject

    def index
      scope = records_scope

      yield_param(:search){
        scope = scope.where Web.name_like_sql(StandardizedCourse, _1)
      }

      yield_param(:education_level_id){
        scope = scope.where education_level_id: _1
      }

      search_resp scope
    end

    def show
      record_get_resp
    end

    def create
      record_create create_params
      record_creation_resp
    end

    def update
      record_get&.update update_params
      record_update_resp
    end

    private

    def records_scope_base
      super.
        includes(:education_level)
    end

    def records_scope_country_filter scope
      scope.where(education_level: { country_id: requested_country_ids })
    end

    def map_record course
      super.update(
        education_level_id: course.education_level.id,
        name: course.name,
        grade: course.grade,
        is_formal: course.is_formal,
        accreditation_authority: course.accreditation_authority,
        lesson_duration: course.lesson_duration,
        attendance_limit: course.attendance_limit,
        preferred_grading: course.preferred_grading,
        description: course.description,
        subjects: course.subjects,
      )
    end

    def record_params
      standardized_course = params.require :standardized_course
      standardized_course.permit(
        %i[ education_level_id grade is_formal lesson_duration
         attendance_limit description],
        name: [], accreditation_authority: [], preferred_grading: [],
        subjects: [:subject_id, :exam, :per_week, grading: []]
      )
    end

    def irregular_association_subject courses
      return if courses.empty?

      ids = courses.map{ _1.subjects || [] }
        .flatten.compact
        .map{ _1['subject_id'] }
        .compact.uniq
      Subject.where country_id: current_user_countries.map(&:id), id: ids
    end

  end
end
