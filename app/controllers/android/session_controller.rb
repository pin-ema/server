# frozen_string_literal: true
require 'securerandom'

module Android
  class SessionController < ApplicationController

    skip_before_action :authenticate, except: %i[logout]

    def login
      reset_session
      @current_user = request.env['warden'].authenticate :password
      reset_session
      if @current_user&.is_privileged? :collector
        current_user.update! token: SecureRandom.uuid
        respond_ok user: map_session
      else
        authorization_fail
      end
    end

    def logout
      current_user.update! token: nil
      respond_ok
    end

    private

    def map_session
      {
        id: current_user.id.to_s,
        login: current_user.login,
        token: current_user.token,
        name: [current_user.full_name_en || '', current_user.full_name || ''],
        country: current_country&.to_b_record,
      }
    end

  end
end
