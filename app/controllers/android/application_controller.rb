# frozen_string_literal: true

module Android
  class ApplicationController < ActionController::API
    wrap_parameters false

    include Server::Actions
    include Server::CurrentUser

    before_action :authenticate

    private

    def authenticate
      auth = params[:auth]
      country = auth&.dig :country
      token = auth&.dig :token
      @current_user = User.find_by country_id: country, token: token
      authentication_fail unless current_user
    end

  end
end
