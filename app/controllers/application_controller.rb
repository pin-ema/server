# frozen_string_literal: true

class ApplicationController < ActionController::API

  include Server::Actions

  def warden_fail_action
    authentication_fail
  end

  if Rails.env.test?
    def test_set_authn
      request.env['warden'].authenticate! :direct_set_for_test
      render plain: 'OK'
    end
  end

end
