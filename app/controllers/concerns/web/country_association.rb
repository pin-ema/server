# frozen_string_literal: true

module Web
  module CountryAssociation
    extend ActiveSupport::Concern

    included do
      add_association :country
    end

    private

    def records_scope
      super.
        includes(:country)
    end

    def map_record record
      super.update(
        country_id: record.country&.id,
      )
    end

  end
end
