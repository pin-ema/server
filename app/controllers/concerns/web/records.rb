# frozen_string_literal: true

module Web
  module Records
    extend ActiveSupport::Concern

    DEFAULT_PER_PAGE_OPTIONS = [ 20, 50, 1 ]

    private

    def record_class_get
      @record_class ||= controller_name.classify.constantize
    end

    def record_create new_params
      @record = record_class_get.create new_params
    end

    def record_get
      unless defined? @record
        @record = record_get_scope.find_by id: params[:id]
      end
      @record
    end

    def record_set record
      @record = record
    end

    def pagination_set scope
      @records_count = scope.count
      @records_page, @records_per_page = pagination_params(
        per_page_options: (@records_per_page && [@records_per_page]) || DEFAULT_PER_PAGE_OPTIONS
      )
    end

    def search_resp scope=records_scope
      pagination_set scope
      scope = scope
        .limit(@records_per_page)
        .offset(( @records_page - 1 ) * @records_per_page)

      if params[:assoc] || params[:abbr]
        records = scope.map{ Records.map_associated_record _1 }
        associations = nil
      else
        records = scope.map{ map_record _1 }
        associations = build_associations scope
      end

      pagination = Web.build_pagination(
        @records_page, @records_count, @records_per_page
      )
      respond_ok(
        records: records,
        associations: associations,
        **pagination,
      )
    end

    def record_get_resp
      record = record_get
      return respond_record_not_found unless record

      respond_ok(
        success: true,
        record: map_record(record),
        associations: build_associations([record]),
      )
    end

    def record_change_resp success
      record = record_get
      return respond_ok success: false unless record

      if record.errors.empty?
        respond_ok success: success, record_id: record.id

      else
        errors = record.errors.map do |error|
          [
            error.attribute,
            [ error.type, error.options[:message] ].compact.join('.'),
          ]
        end
        respond_ok success: success, errors: errors

      end
    end

    def record_creation_resp
      record_change_resp record_get&.persisted?
    end

    def record_update_resp
      record = record_get
      record_change_resp record && !record.changed?
    end

    def records_scope_base
      record_class_get.all.order(:id)
    end

    def record_get_scope
      records_scope_base
    end

    def records_scope
      records_scope_base
    end

    def record_params
      {}
    end

    def create_params
      record_params
    end

    def update_params
      record_params
    end

    def map_record record
      { id: record.id }
    end

    def build_associations records
      associations = {}
      self.class.associations.each do |name|
        index = {}

        records.each do |record|
          association = record.send name
          next if association.nil? || index.key?(association.id)
          index[association.id] = Records.map_associated_record association
        end

        associations[name] = index.values
      end

      self.class.irregular_associations.each do |name|
        index = {}

        associated_records = send "irregular_association_#{name}", records
        associated_records&.each do |association|
          index[association.id] = Records.map_associated_record association
        end

        associations[name] = index.values
      end

      associations.presence
    end

    module ClassMethods

      def associations
        @associations ||= []
      end

      def add_association name
        associations.push name
      end

      def irregular_associations
        @irregular_associations ||= []
      end

      def add_irregular_association name
        irregular_associations.push name
      end

    end

    def self.reduce_associated_records records
      index = records.each_with_object({}) do |record, memo|
        memo[record.id] = Records.map_associated_record record
      end
      index.values
    end

    def self.map_associated_record record
      {
        id: record.id,
        labels: record.try(:labels),
      }
    end

  end
end
