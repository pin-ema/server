# frozen_string_literal: true

module Web
  module CountryBoundary
    extend ActiveSupport::Concern

    def requested_country_ids
      unless defined? @requested_country_ids
        requested_id = params[:country_id].presence&.to_i
        @requested_country_ids = if requested_id
          current_user_countries.find{ _1.id == requested_id }
        else
          current_user_countries.map(&:id)
        end
      end
      @requested_country_ids
    end

    private

    # TODO remove
    def record_get_scope
      scope = super
      records_scope_country_filter scope
    end

    def records_scope
      scope = super
      records_scope_country_filter scope
    end

    def records_scope_country_filter scope
      scope.where country: requested_country_ids
    end

  end
end
