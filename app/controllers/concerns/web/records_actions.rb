module Web::RecordsActions
  extend ActiveSupport::Concern

  PER_PAGE_OPTIONS = [ 20, 50, 1 ]

  def index
    scope = filter_records(records_scope)
    associations = nil

    records, pagination = if params[:id].present?
      [
        [ scope.first ],
        Web.build_pagination(1, 1, 1)
      ]

    else
      count = scope.count
      page, per_page = pagination_params(
        per_page_options: PER_PAGE_OPTIONS
      )
      [
        scope
          .limit(per_page)
          .offset(( page - 1 ) * per_page)
          .to_a,
        Web.build_pagination(page, count, per_page)
      ]

    end

    if params[:abbr]
      records.map!(&:to_b_record)

    else
      records.map!{ map_record _1 }
      associations = records_associations(records)
        &.to_a.each_with_object({}) do |association, memo|
        name, associated_records = association
        memo[name] = associated_records.map(&:to_b_record)
      end

    end

    respond_ok(
      records: records,
      associations: associations,
      **pagination,
    )
  end

  def create
    @record = record_class.create create_params

    if @record.persisted? && @record.errors.empty?
      respond_ok success: true, record_id: @record.id

    else
      respond_ok errors: @record.get_errors

    end
  end

  def update
    find_record

    if @record&.update(update_params) && @record.errors.empty?
      respond_ok success: true

    else
      respond_ok errors: @record&.get_errors

    end
  end

  def archive
    find_record
    if @record&.update archived: true
      respond_ok success: true

    else
      respond_ok

    end
  end

  private

  def record_class
    @record_class ||= controller_name.classify.constantize
  end

  def find_record
    @record = records_scope.find_by id: params[:id]
  end

  def records_scope_base
    record_class.all.order(:id, :desc)
  end

  def records_scope
    records_scope_base
  end

  def filter_records scope
    yield_param(:id){ scope = scope.where id: _1 }
    scope
  end

  def map_record record
    { id: record.id }
  end

  def records_associations records
    nil
  end

  def record_params
    {}
  end

  def create_params
    record_params
  end

  def update_params
    record_params
  end

end
