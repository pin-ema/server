# README
write me

#### is it any good?
[yes](https://news.ycombinator.com/item?id=3067434)

# DEV GUIDES

### board
`bin/rails db:drop db:create db:migrate`

`bin/rails demo:up`

`bundle exec erd && xdg-open erd.pdf`

`psql -h localhost -p 3075 -U postgres ema_development`



#### compote dev-ops
```shell
# server console
sudo bin/compose run --rm app bash

# dump
pg_dump \
    -v -x \
    -h pg -U postgres -d ema_production \
    -f /var/stack_tmp/ema-db_2023-02-26-v3.dump

# import
psql \
    -h localhost -p 3075 -U postgres \
    -d ema_development \
    < tmp/ema-db_2023-02-26-v3.dump

# restore production
psql \
  -h pg -U postgres -d ema_production \
  < /var/stack_tmp/ema-db_2023-02-26-v3.dump
```

#### demo
`bin/rails demo:up`
`bin/rails demo:db:load:syria`

#### dev - up

`bin/compose up`

`bin/rails s`

`bin/frontend pack -w`
