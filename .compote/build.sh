set -e

mount_app="-v $stack_path/src:/app"
mount_gems="-v $stack_path/var/ruby_bundle:/usr/local/bundle"
mount_assets="-v $stack_path/var/assets:/app/public/assets"

libops_print "compiling release images"

libops_print -p "updating ruby gems"
libops_docker_run app:base \
    "$mount_app $mount_gems" \
    "bash /app/.compote/lib/bundle_install.sh"

libops_print -p "compiling assets"
libops_docker_run app:base \
  "$mount_app $mount_gems $mount_assets" \
  "rails assets:precompile"

libops_print -p "building"
bin/build_image app release
