# frozen_string_literal: true

require 'fileutils'
require 'pathname'
require 'erb'
require 'dotenv/load'

tmp_path = Pathname.new 'tmp/compote'
FileUtils.mkdir_p tmp_path

pg_data_path = tmp_path.join 'pg_data'
FileUtils.mkdir_p pg_data_path

erb = ERB.new(File.read '.compote/dev/proxy.conf.erb')
File.write tmp_path.join('proxy.conf').to_path, erb.result

puts 'requires sudo for docker'
`sudo systemctl -q is-active docker || sudo systemctl -q start docker`
sleep 1

command = [
  'sudo docker compose',
  '--env-file .env',
  '-f .compote/dev/docker-compose.yml',
  "--project-directory #{Dir.pwd}",
  '-p ema-local',
  'up'
].join(" \\\n  ")
puts 'running:'
puts command
exec command
