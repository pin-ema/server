set -e

bundle config set --local without 'development test'
bundle install
